# -*- coding: utf-8 -*-
"""
Created on Wed May 27 13:53:41 2020

@author: Ismahene
"""

import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
import scipy.signal as signal
import collections 
import heapq
import re

def read_data_csv(csv_file):
    """
    

    Parameters
    ----------
    csv_file : txt tabulated file
        the headers of the file have to be the following: 
            timeStep : time step
            F(x): smd force
            F(-x): tcl force
            velocityInums   : velocity
            direction       : direction of pulling
            kInpNnm: spring contant in pN/nm


    Returns
    -------
    df : pandas df
        dataframe of all information.

    """
    df = pd.read_csv (csv_file, sep='\t')
    return df
    
def extract_info_from_smd_log (log_file):
    
    output_file='log.txt'
    output_energies= 'energies.txt'
    dico={}
    force_tcl=[]
    energy_parms={}
    F_tcl=[]
    with open(log_file,"r") as file_one , open(output_file, "w") as output,  open(output_energies, "w") as outputE :
        patrn = "SMD  "
        for line in file_one:
            if re.search(patrn, line):
                output.write(line)
                
            if re.search('Info: TIMESTEP', line):
               # print(line)
                TS=int(line.split(' ')[-1])
              #  print(TS)
            if  re.search('SMD DIRECTION ', line):
                direction=line.split(' ')
                direction=np.array([float(direction[5]),float(direction[6]),float(direction[7])])
                #reshape matrix to be able to do the dot product later
                direction.reshape(3,1)
                print('direction of pulling [x, y, z]: ', direction)

            if re.search('ENERGY:', line) and 'Info:' not in line:
                outputE.write(line)

            if re.search('SMD VELOCITY', line ):
                velocity=line.split()
                smd_vel=float(velocity[3]) / TS *10**15*10**-10   #smd velocity in nm/ns
                print('pulling speed: ', smd_vel, "nm/ns")
                
                #extract spring constant and convert it 
            if re.search('SMD K' , line ) and 'K2' not in line :
                 smd_k_namd=line.split()
                
                 smd_k=float(smd_k_namd[3])*69.479*10 #convert pN/nm
            
            if re.search('TCL', line) and ('Suspending') not in line and ('Running') not in line and ('ACTIVE') not in line and ('FILE') not in line :
                F_tcl= line.split()[-1]
                force_tcl.append(float(F_tcl)*-69.479) #add force of pulling into dictionnary of information

    raw_df= pd.read_csv(output_file,sep=' ', header=None)
    raw_df_energies= pd.read_csv(output_energies, skipinitialspace=True, sep=' ', engine='python', header=None)

    F= np.dot(raw_df.iloc[:, [6,7,8]] ,direction)
    dico["F[pN]"]=F
    dico['timeStep']= raw_df.iloc[:, 2]
    dico['timeInS']= dico['timeStep'] * TS * 10**-15  #two fem to seconds
    dico['displacement[nm]']=  dico['timeInS'] *10**9 *smd_vel #time in ns multiplied by velocity (nm/ns)

    df = pd.DataFrame.from_dict(dico)
    if len(force_tcl) != 0:
        df.loc[:,'-F[pN]'] = pd.Series(force_tcl)
            
    df.loc[:,'velocitynmns'] = pd.Series(round(smd_vel,4))
    df.loc[:,'direction'] = pd.Series(direction)

    df.loc[:,'kInpNnm'] = pd.Series(smd_k)
    energy_parms['timeStep']= raw_df_energies.iloc[:,1]
    energy_parms['BOND']= raw_df_energies.iloc[:,2]
    energy_parms['ANGLE']= raw_df_energies.iloc[:,3]
    energy_parms['DIHED']= raw_df_energies.iloc[:,4]
    energy_parms['IMPRP']= raw_df_energies.iloc[:,5]
    energy_parms['ELECT']= raw_df_energies.iloc[:,6]
    energy_parms['VDW']= raw_df_energies.iloc[:,7]
    energy_parms['BOUNDARY']= raw_df_energies.iloc[:,8]
    energy_parms['MISC']= raw_df_energies.iloc[:,9]
    energy_parms['KINETIC']= raw_df_energies.iloc[:,10]
    energy_parms['TOTAL']= raw_df_energies.iloc[:,11]
    energy_parms['TEMP']= raw_df_energies.iloc[:,12]
    energy_parms['POTENTIAL']= raw_df_energies.iloc[:,13]
    energy_parms['TOTAL3']= raw_df_energies.iloc[:,14]
    energy_parms['TEMPAVG']= raw_df_energies.iloc[:,15]
    energy_parms['PRESSURE']= raw_df_energies.iloc[:,16]
    energy_parms['GPRESSURE']= raw_df_energies.iloc[:,17]
    energy_parms['VOLUME']= raw_df_energies.iloc[:,18]
    energy_parms['PRESSAVG']= raw_df_energies.iloc[:,19]
    energy_parms['GPRESSAVG']= raw_df_energies.iloc[:,20]

    energy_parms['timeInS']= energy_parms['timeStep'] * TS* 10**-15
    df_eparms = pd.DataFrame.from_dict(energy_parms)
    return df_eparms, df, smd_vel
def get_RMS(data):
    """
    Parameters
    ----------
    data : array 
        array of data

    Returns
    -------
    RMS: Root mean square of data
    """
    rms = np.sqrt(np.mean(np.array(data)**2))

    return  rms

def downsampling(data):
    
    """
    Parameters
    ----------
    data : array
        array of data.

    Returns
    -------
    Panda data frame of downsampled data
        

    """
    data=np.array(data)
    l=len(data) #length of data
    new_sample=[]
    for i in range(0, l , 10):
        new_sample.append(data[i:i+10])
    new_sample=pd.DataFrame(new_sample)
    return new_sample[0]
    

def get_natural_cubic_spline_model(x, y, minval=None, maxval=None, n_knots=None, knots=None):
    """
    Get a natural cubic spline model for the data.

    For the knots, give (a) `knots` (as an array) or (b) minval, maxval and n_knots.

    If the knots are not directly specified, the resulting knots are equally
    space within the *interior* of (max, min).  That is, the endpoints are
    *not* included as knots.

    Parameters
    ----------
    x: np.array of float
        The input data
    y: np.array of float
        The outpur data
    minval: float 
        Minimum of interval containing the knots.
    maxval: float 
        Maximum of the interval containing the knots.
    n_knots: positive integer 
        The number of knots to create.
    knots: array or list of floats 
        The knots.

    Returns
    --------
    model: a model object
        The returned model will have following method:
        - predict(x):
            x is a numpy array. This will return the predicted y-values.
    """

    if knots:
        spline = NaturalCubicSpline(knots=knots)
    else:
        spline = NaturalCubicSpline(max=maxval, min=minval, n_knots=n_knots)

    p = Pipeline([
        ('nat_cubic', spline),
        ('regression', LinearRegression(fit_intercept=True))
    ])

    p.fit(x, y)

    return p


class AbstractSpline(BaseEstimator, TransformerMixin):
    """Base class for all spline basis expansions."""

    def __init__(self, max=None, min=None, n_knots=None, n_params=None, knots=None):
        if knots is None:
            if not n_knots:
                n_knots = self._compute_n_knots(n_params)
            knots = np.linspace(min, max, num=(n_knots + 2))[1:-1]
            max, min = np.max(knots), np.min(knots)
        self.knots = np.asarray(knots)

    @property
    def n_knots(self):
        return len(self.knots)

    def fit(self, *args, **kwargs):
        return self


class NaturalCubicSpline(AbstractSpline):
    """Apply a natural cubic basis expansion to an array.
    The features created with this basis expansion can be used to fit a
    piecewise cubic function under the constraint that the fitted curve is
    linear *outside* the range of the knots..  The fitted curve is continuously
    differentiable to the second order at all of the knots.
    This transformer can be created in two ways:
      - By specifying the maximum, minimum, and number of knots.
      - By specifying the cutpoints directly.  

    If the knots are not directly specified, the resulting knots are equally
    space within the *interior* of (max, min).  That is, the endpoints are
    *not* included as knots.
    Parameters
    ----------
    min: float 
        Minimum of interval containing the knots.
    max: float 
        Maximum of the interval containing the knots.
    n_knots: positive integer 
        The number of knots to create.
    knots: array or list of floats 
        The knots.
    """

    def _compute_n_knots(self, n_params):
        return n_params

    @property
    def n_params(self):
        return self.n_knots - 1

    def transform(self, X, **transform_params):
        X_spl = self._transform_array(X)
        if isinstance(X, pd.Series):
            col_names = self._make_names(X)
            X_spl = pd.DataFrame(X_spl, columns=col_names, index=X.index)
        return X_spl

    def _make_names(self, X):
        first_name = "{}_spline_linear".format(X.name)
        rest_names = ["{}_spline_{}".format(X.name, idx)
                      for idx in range(self.n_knots - 2)]
        return [first_name] + rest_names

    def _transform_array(self, X, **transform_params):
        X = X.squeeze()
        try:
            X_spl = np.zeros((X.shape[0], self.n_knots - 1))
        except IndexError: # For arrays with only one element
            X_spl = np.zeros((1, self.n_knots - 1))
        X_spl[:, 0] = X.squeeze()

        def d(knot_idx, x):
            def ppart(t): return np.maximum(0, t)

            def cube(t): return t*t*t
            numerator = (cube(ppart(x - self.knots[knot_idx]))
                         - cube(ppart(x - self.knots[self.n_knots - 1])))
            denominator = self.knots[self.n_knots - 1] - self.knots[knot_idx]
            return numerator / denominator

        for i in range(0, self.n_knots - 2):
            X_spl[:, i+1] = (d(i, X) - d(self.n_knots - 2, X)).squeeze()
        return X_spl




def find_peaks(f,ts, knots, **kwargs):
    """
    Parameters
    ----------
    ts : table of info extracted from log (type= pandas.df)
    knots : int
        The knots
    **kwargs : int
        The threshold of prominence.

    Returns
    -------
    df : data frame
        data frame of data.
    smooth : array of smooth data
        
    peaks:list
        list of all detected peaks
    prominences : array
        array of all prominences 

    """
    
   # energy_parms,df=extract_info_from_smd_log(input_file)
    prominence = kwargs.get('prominence', None)
    model = get_natural_cubic_spline_model(ts, f, minval=min(ts), maxval=max(ts), n_knots=knots)
    smooth = model.predict(ts)
    #peaks detection using find_peaks function from scipy signal library
    peaks=signal.find_peaks(smooth,prominence=prominence)[0] #using find peaks from signal
    prominences = signal.peak_prominences(smooth, peaks)[0]
    
    return smooth, list(peaks), prominences

def get_slopes(displacement, ts, f, peaks, distance):
    """
    Parameters
    ----------
    displacement : pd dataframe
        contains the displacement in nanometer.
    ts : pd dataframe
        time in seconds .
    f : pd dataframe
        force in pN.
    peaks : list
        list of peaks ( the indexes of peaks).
    distance : float
        distance in nanometer set in the entry of the interface by the user.

    Returns
    -------
    d : dict
        dictionnary of information.
    slope : list
        list containing the loading rates the slopes.
    intercept : list
        list containing the intercepts of the slopes.

    """

    displacement=np.array(displacement)
    d={'f':[], 'ts':[], 'dis1':[], 'disTMP':[], 'dis2':[], 'index_dis2':[], 'peaks':[]}
    #iterate over all peaks
    for i in peaks:
        d['f'].append(f[i])
        d['ts'].append(ts[i])
        #compute displacement using corresponding peak index
        d['dis1'].append(displacement[i])
    #compute selected value = displacement[peak_index]-distance given by the user in nm
    for i in range(len(peaks)):
        d['disTMP'].append(d['dis1'][i]-distance*1)

    #search for closest value to selected one (selected one is distance before peak)
    for i in range(len(peaks)):
        d['dis2'].append(displacement.flat[np.abs(displacement - d['disTMP'][i]).argmin()])
    #put found values in dictionnary of info
    for i in range(len(peaks)):
        d['index_dis2'].append(int(list(displacement).index(d['dis2'][i])))
        
    for i in peaks:
        d['peaks'].append(int(i))

    slope=[]
    intercept=[]
    
    #slopes and intercept computing
    for i in range(len(peaks)):
        slope.append(np.polyfit(np.array(ts[d['index_dis2'][i]:d['peaks'][i]]),np.array(f[d['index_dis2'][i]:d['peaks'][i]]), 1)[0])
        intercept.append(np.polyfit(np.array(ts[d['index_dis2'][i]:d['peaks'][i]]),np.array(f[d['index_dis2'][i]:d['peaks'][i]]), 1)[1])
    return d, slope, intercept


def slopes_at_peaks(displacement, ts, f, peaks, distance, timeStep, velocity):
    """
    

    Parameters
    ----------
    displacement : pd dataframe
        displacement in nanometer from the log file of SMD simulation
    ts : pd dataframe
        time in seconds from the log file of simulation.
    f : pd dataframe
        force in pN.
    peaks : list
        list containing all peaks.
    distance : float
        distance in nanometer (set by the user in the interface).
    timeStep : timesteps
         time steps from the log file of simultion.

    Returns
    -------
    d: pd dataframe
    dataframe of information, ready to export as csv

    """
    slopes=get_slopes(displacement, ts, f, peaks, distance)[1]

    d={'':[], 'timeStep': [], 'timeInS':[], 'UnfoldingForce[pN]':[], 'UnfoldingLoadingRate[pN/s]':'','Stiffness[pN/nm]':[], 'UnfoldingDisplacement[nm]': [] }
    
    d['UnfoldingLoadingRate[pN/s]']=slopes
    for i in range(len(peaks)):
        d[''].append(str(i+1))
    for slope in slopes:    
        d['Stiffness[pN/nm]'].append(slope/velocity)
#COMPUTING DISTANCES
    d['UnfoldingForce[pN]']=np.array(f[peaks])

    d['UnfoldingDisplacement[nm]']=np.array(displacement[peaks])
    distances = [displacement[peaks[i]] - displacement[peaks[i-1]] for i in np.arange(1, len(peaks))] 
    d_dist={}
    d_dist['InterDomainDistance(nm)']=distances
    d['timeStep']=np.array(timeStep[peaks])
    d['timeInS']=np.array(ts[peaks])
   # print(d)
    d = pd.DataFrame.from_dict(d)
    d.loc[:,'InterDomainDistance(nm)'] = pd.Series(d_dist['InterDomainDistance(nm)'])
    d.loc[:,'LoadingRate at distance of(nm)'] = distance

    return d 


            
    
def compute_slopes_second_detection(df,force, velocity, distance, peak_index_list, selection_list, extensions):
    """
    Parameters
    ----------
    input_file : log file of SMD simulation.
    
    peak_index_list :  list
        the indexes selected by the user.

    Returns
    -------
    - df: data frame of information (timeStep, timeInS, UnfoldingForce[pN], UnfoldingLoadingRate, UnfoldingDisplacement)
    -slope: array : loading rates
    -intercept: array: intercept of slopes

    """
    displacement= np.array(df['displacement[nm]'])
    timeIns= df['timeInS']
    timeStep= df['timeStep']

    # compute the distances, ds, between points
    dx, dy = extensions[+1:]-extensions[:-1],  extensions[+1:]-extensions[:-1]
    ds = np.array((0, *np.sqrt(dx*dx+dy*dy)))

    # compute the total distance from the 1st point, measured on the curve
    s = np.cumsum(ds)

    # interpolate using 200 point
    extensions_xinter = np.interp(np.linspace(0,s[-1], len(df['timeStep'])), s, extensions)

    peak_index_list=sorted(peak_index_list)
    d={'timeInS': [], 'slopes':[], 'disTMP':[],  'dis2':[], 'index_dis2':[], 'peaks':[], 'f':[], 'dis1':[]}
    for i in range(len(peak_index_list)):
        d['timeInS'].append(timeIns[peak_index_list[i]])
        d['f'].append(force[peak_index_list[i]])
        d['dis1'].append(displacement[peak_index_list[i]])

    # for i in range(len(d['timeInS'])):
    #     d['distance'].append(d['timeInS'][i]* velocity) #distance is in nm
        
    for i in range(len(peak_index_list)):
        d['disTMP'].append(d['dis1'][i]-distance)

    #search for closest value to selected one (selected one is distance before peak)
        d['dis2'].append(displacement.flat[np.abs(displacement - d['disTMP'][i]).argmin()])
    #put found values in dictionnary of info
        d['index_dis2'].append(int(list(displacement).index(d['dis2'][i])))
        
    for i in peak_index_list:
        d['peaks'].append(int(i))

    slope=[]
    intercept=[]
    
    #slopes and intercept computing
    for i in range(len(peak_index_list)):
      #  print(timeIns[d['index_dis2'][i]:d['peaks'][i]])
        slope.append(np.polyfit(np.array(timeIns[d['index_dis2'][i]:d['peaks'][i]]),np.array(force[d['index_dis2'][i]:d['peaks'][i]]), 1)[0])
        intercept.append(np.polyfit(np.array(timeIns[d['index_dis2'][i]:d['peaks'][i]]),np.array(force[d['index_dis2'][i]:d['peaks'][i]]), 1)[1])
        
    dico={'Peak':[], 'timeStep': [], 'timeInS':[], 'UnfoldingForce[pN]':[], 'UnfoldingLoadingRate[pNsec]':'', 'UnfoldingDisplacement[nm]': [], 
           'extensions[nm]': []}
    dico['UnfoldingLoadingRate[pNsec]']=slope
    keys=[]
    l=[]
    for i in range(len(peak_index_list)):
        dico['Peak'].append(str(i+1))
    # for i in range(len(selection_list)):
    #     dico['selection'].append(selection_list[i])
        
    # numerator = {i:0 for i in set(selection_list)}
    # denominator = collections.Counter(selection_list)

  
    # for v in selection_list:
    #     numerator[v] += 1
    #     dico['UnfoldingNumber'].append(str(numerator[v]) + '/' + str(denominator[v]))

    
#COMPUTING DISTANCES
    dico['UnfoldingForce[pN]']=np.array(force[peak_index_list])

    dico['UnfoldingDisplacement[nm]']=np.array(displacement[peak_index_list]) #in nm
    dico['extensions[nm]']= np.array(extensions_xinter[peak_index_list]) #in nm
    distances = [displacement[peak_index_list[i]] - displacement[peak_index_list[i-1]] for i in np.arange(1, len(peak_index_list))] 

    d_dist={}
    d_dist['InterDomainDistance(nm)']=distances
    dico['timeStep']=np.array(timeStep[peak_index_list])
    dico['timeInS']=np.array(timeIns[peak_index_list])
   # print(dico)
    
    df_data = pd.DataFrame.from_dict(dico)
    df_data.loc[:,'InterDomainDistance(nm)'] = pd.Series(d_dist['InterDomainDistance(nm)'])

    return df_data , slope, intercept

def find_roots(x, y):
    """
    This function finds the intersection points between two 
    curves x and y

    Parameters
    ----------
    x : numpy matrix
        the first function ( all the points of the function).
    y : numpy matrix
        the second function (curve) .

    Returns
    -------
    roots
        numpy matrix containing roots .

    """
    s = np.abs(np.diff(np.sign(y))).astype(bool)
    return x[:-1][s] + np.diff(x)[s] / (np.abs(y[1:][s] / y[:-1][s]) + 1)


def extract_N_largest_keys(test_dict, N):
    largest_keys = heapq.nlargest(N, test_dict, key=lambda k: test_dict[k])
    largest_key_value_pairs = [(key, test_dict[key]) for key in largest_keys]
    return largest_key_value_pairs
