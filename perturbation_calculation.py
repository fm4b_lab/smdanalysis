#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 28 09:09:50 2024

@author: mesbah
"""

import numpy as np
import matplotlib.pyplot as plt
from prody import *
import argparse

def calculate_distance(coord1, coord2):
    """
    Parameters
    ----------
    coord1 : numpy matrix of  x , y , z coordinates of first residue
        DESCRIPTION.
    coord2 : numpy matrix of coordinates of second residue
        DESCRIPTION.

    Returns
    -------
    TYPE
        matrix numpy of eucleadian distance between the two residues

    """
    #Eucleadian distance between two coordinates of two atoms
    return np.linalg.norm(coord1 - coord2)


def calculate_center_of_mass_trajectory(pdb_path, dcd_path, residue_ids):
    # Load the protein structure from the PDB file
    structure = parsePDB(pdb_path)

    # Select only PROTEIN atoms, NO IONS, NO WATER, and calcium
    protein = structure.select('protein or resname CAL')

    residue_names = []
    for atom in protein:
        residue_names.append(atom.getResname())

    # Load the trajectory from the DCD file
    trajectory = parseDCD(dcd_path)

    com_trajectories = {}

    for residue_id in residue_ids:
        residue = protein.select(f'resid {residue_id} or resname CAL')
        atom_indices = residue.getIndices()

        # Calculate the mass of each atom
        masses = np.array([atom.getMass() for atom in residue])

        com_trajectory = []

        for frame in trajectory:
            coords = frame.getCoords()
            residue_coords = coords[atom_indices]

            # Calculate the mass-weighted center of mass of the residue
            com = np.sum(residue_coords * masses[:, np.newaxis], axis=0) / np.sum(masses)
            com_trajectory.append(com)

        com_trajectories[residue_id] = com_trajectory

    return com_trajectories, residue_names


def calculate_distances_between_residues(pdb_path, dcd_path, residue_ids, start_frame):
    """
    Calculate distances between residue pairs at a given frame.
    Optimized to avoid redundant distance calculations.
    """
    # Calculate the center of mass trajectory for each residue
    com_trajectories, residue_names = calculate_center_of_mass_trajectory(pdb_path, dcd_path, residue_ids)

    distances = {}
    computed_pairs = {}

    for i in range(len(residue_ids)):
        for j in range(i + 1, len(residue_ids)):  # Only calculate for i < j
            res1, res2 = residue_ids[i], residue_ids[j]

            # Check if the distance for this pair is already computed
            if (res1, res2) in computed_pairs or (res2, res1) in computed_pairs:
                continue

            com1_start = com_trajectories[res1][start_frame]
            com2_start = com_trajectories[res2][start_frame]

            distance_start = calculate_distance(com1_start, com2_start)

            # Store the distance in both orderings
            distances[(res1, res2)] = distance_start
            computed_pairs[(res1, res2)] = True
            computed_pairs[(res2, res1)] = True

    return distances, residue_names

def filter_residues_by_distance(distances, selected_residue, threshold):
    """
    Filter residue pairs where the initial distance is below the threshold.
    """
    filtered_residues = {}

    for (res1, res2), distance in distances.items():
        if selected_residue in (res1, res2) and distance < threshold:
            filtered_residues[(res1, res2)] = distance

    return filtered_residues

def compute_distance_change_over_frames(pdb_path, dcd_path, filtered_residues, frame_start, frame_end, ld, distance_change_threshold, log_file):
    """
    Compute distance change metric for residue pairs over frames and log exceeding changes.
    """
    com_trajectories, _ = calculate_center_of_mass_trajectory(pdb_path, dcd_path, list(set([r for pair in filtered_residues for r in pair])))

    total_metric = 0.0
    nb = len(filtered_residues)

    for (res1, res2), initial_distance in filtered_residues.items():
        com1_start = com_trajectories[res1][frame_start]
        com2_start = com_trajectories[res2][frame_start]
        com1_end = com_trajectories[res1][frame_end]
        com2_end = com_trajectories[res2][frame_end]

        distance_start = calculate_distance(com1_start, com2_start)
        distance_end = calculate_distance(com1_end, com2_end)
        diff_squared = (distance_start - distance_end) ** 2

        # Check if the distance change exceeds the threshold
        if np.abs(distance_start - distance_end) > distance_change_threshold:
            log_message = (
                f"{res1}-{res2:<13} | {frame_start}-{frame_end:<14} | {np.abs(distance_start - distance_end):.2f} Å\n"
            )
            log_file.write(log_message)
            print("-----------------------------------------------------")
            print(log_message)


        total_metric += np.exp(-diff_squared / (ld ** 2))

    return 1 - (1 / nb) * total_metric if nb > 0 else 0.0

def main():
    parser = argparse.ArgumentParser(description="Compute residue interaction metrics from PDB and DCD files.")
    
    # Arguments
    parser.add_argument("--pdb", required=True, help="Path to the PDB file")
    parser.add_argument("--dcd", required=True, help="Path to the DCD file")
    parser.add_argument("--residue_range", required=True, help="Residue range (e.g., '1-90')")
    parser.add_argument("--frame_start", type=int, required=True, help="Start frame number")
    parser.add_argument("--frame_end", type=int, required=True, help="End frame number")
    parser.add_argument("--frame_interval", type=int, required=True, help="Frame interval (N)")
    parser.add_argument("--threshold", type=float, required=True, help="Threshold for residue distance")
    parser.add_argument("--distance_change_threshold", type=float, required=True, help="Threshold for distance change")
    parser.add_argument("--output_txt", required=True, help="Path to the output text log file")
    parser.add_argument("--output_png", required=True, help="Path to the output PNG file")
    
    args = parser.parse_args()

    # Parse residue range
    residue_range = [int(x) for x in args.residue_range.split('-')]
    residues = list(range(residue_range[0], residue_range[1] + 1))
    
    pdb_path = args.pdb
    dcd_path = args.dcd
    start_frame = args.frame_start
    end_frame = args.frame_end
    N = args.frame_interval
    threshold = args.threshold
    distance_change_threshold = args.distance_change_threshold
    log_file_path = args.output_txt

    # Compute metrics and generate matrix
    num_segments = (end_frame - start_frame) // N
    metrics_matrix = np.zeros((num_segments, len(residues)))  # Rows = segments, Columns = residues
    residue_map = [["" for _ in residues] for _ in range(num_segments)]  # Store residue IDs for visualization

    with open(log_file_path, "w") as log_file:
        for res_idx, res in enumerate(residues):
            print("-----------------------------------------------------")
            print("Residue:", res)
            print("-----------------------------------------------------")

            distances, residue_names = calculate_distances_between_residues(pdb_path, dcd_path, residues, start_frame)
            filtered_residues = filter_residues_by_distance(distances, res, threshold)
            data = filtered_residues.keys()
            print(data)
            log_file.write('all residues potentially interacting with residue ' + str(res) + ' below threshold ' + str(threshold) + ' at frame 0 \n')
            log_file.write('\t'.join('{} {}'.format(x[0], x[1]) for x in data))
            log_file.write("\n")

            log_file.write("Residue Pairs with Distance Change Exceeding Threshold" + str(distance_change_threshold) + "\n")
            log_file.write("-----------------------------------------------------\n")
            log_file.write("Residue Pair    | Frame Range       | Distance Change (Å)\n")
            log_file.write("-----------------------------------------------------\n")
            for segment_idx, frame_start in enumerate(range(start_frame, end_frame, N)):
                frame_end = frame_start + N
                print("-----------------------------------------------------")
                print(f"Frames: {frame_start} to {frame_end}")
                print("-----------------------------------------------------")

                # Compute metric for this segment and log significant changes
                total_metric = compute_distance_change_over_frames(
                    pdb_path, dcd_path, filtered_residues, frame_start, frame_end, 0.38, distance_change_threshold, log_file
                )
                metrics_matrix[segment_idx, res_idx] = total_metric

                # Store considered residue IDs
                residue_map[segment_idx][res_idx] = ", ".join(str(r[1] if r[0] == res else r[0]) for r in filtered_residues.keys())

    # Plot the chimograph
    plt.figure(figsize=(12, 8))
    plt.imshow(metrics_matrix, aspect='auto', cmap='viridis', interpolation='nearest', vmin=0, vmax=1)

    plt.xlabel("Residues")
    plt.ylabel("Frame Intervals")
    plt.title("Perturbation Measure Over Frame Intervals and Residues")
    plt.colorbar(label="Total Metric")

    # Adjust axis ticks
    plt.xticks(ticks=np.arange(len(residues)), labels=residues, rotation=90)
    frame_intervals = [f"{start}-{start + N}" for start in range(start_frame, end_frame, N)]
    plt.yticks(ticks=np.arange(num_segments), labels=frame_intervals)

    plt.tight_layout()
    plt.savefig(args.output_png)
    plt.show()

    print(f"Residue distance change log written to: {log_file_path}")

if __name__ == "__main__":
    main()

