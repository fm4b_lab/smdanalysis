#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  26 00:29:48 2023

@author: Ismahene Mesbah
"""

import prody as prd
import numpy as np
import subprocess

def calculate_distance(coord1, coord2):
    """
    Parameters
    ----------
    coord1 : numpy matrix of  x , y , z coordinates of first residue
        DESCRIPTION.
    coord2 : numpy matrix of coordinates of second residue
        DESCRIPTION.

    Returns
    -------
    TYPE
        matrix numpy of eucleadian distance between the two residues

    """
    #Eucleadian distance between two coordinates of two atoms
    return np.linalg.norm(coord1 - coord2)

def calculate_center_of_mass_trajectory(pdb_path, dcd_path, residue_ids):
    # Load the protein structure from the PDB file
    structure = prd.parsePDB(pdb_path)

    # Select only PROTEIN atoms, NO IONS, NO WATER, and calcium
    protein = structure.select('protein or resname CAL')

    residue_names = []
    for atom in protein:
        residue_names.append(atom.getResname())

    # Load the trajectory from the DCD file
    trajectory = prd.parseDCD(dcd_path)
    
    com_trajectories = {}
    
    for residue_id in residue_ids:
        residue = protein.select(f'resid {residue_id} or resname CAL')
        atom_indices = residue.getIndices()
        
        # Calculate the mass of each atom
        masses = np.array([atom.getMass() for atom in residue])
        
        com_trajectory = []
        
        for frame in trajectory:
            coords = frame.getCoords()
            residue_coords = coords[atom_indices]
            
            # Calculate the mass-weighted center of mass of the residue
            com = np.sum(residue_coords * masses[:, np.newaxis], axis=0) / np.sum(masses)
            com_trajectory.append(com)
        
        com_trajectories[residue_id] = com_trajectory

    return com_trajectories, residue_names

def calculate_distances_between_residues(pdb_path, dcd_path, residue_ids, start_frame, end_frame):
    """
    Parameters
    ----------
    pdb_path : str
        path to pdb
    dcd_path :  str
        path to dcd
    residue_ids : list
        list of residue ids
    start_frame : int
        index of frame at which you want to start calculation
    end_frame : int
        index of frame at which you want to end calculation

    Returns
    -------
    distances : dctionary of distances between pairs of residues
        key is tuple: pair of residue
        value is distance in angstrom
    residue_names : list
        list of residue names

    """
    # Calculate the center of mass trajectory for each residue
    com_trajectories, residue_names = calculate_center_of_mass_trajectory(pdb_path, dcd_path, residue_ids)

    # Generate pairs of residues
    residue_pairs = []
    for i in range(len(residue_ids)):
        for j in range(i+1, len(residue_ids)):
            #store pairs in list: residue i and residue i-1
            residue_pairs.append((residue_ids[i], residue_ids[j]))
    #init dict of distances 
    distances = {}
    # loop over resdiue pairs
    for residue_pair in residue_pairs:
        # get number (id) of res1 and res2 from tuple
        res1, res2 = residue_pair
        # get center of mass of residue 1
        com_trajectory1 = com_trajectories[res1]
        #get center of mass of residue 2
        com_trajectory2 = com_trajectories[res2]

        # Calculate the distance between center of mass for each frame within the specified range
        distance_trajectory = []
        # loop over selected frames
        for frame_num in range(start_frame, end_frame):
            #get center of mass at that specefic frame for both residues
            com1 = com_trajectory1[frame_num]
            com2 = com_trajectory2[frame_num]
            #calculate eucleadian distance between the two centers of mass
            distance = calculate_distance(com1, com2)
            #store the found distance at each frame in distance_trajectory list
            distance_trajectory.append(distance)

        #fill the dict in the following way: key is pair of residues (tuple) and the distances at each frame for that pair
        distances[residue_pair] = distance_trajectory

    return distances, residue_names

from scipy.interpolate import interp1d

def resample_force(force_values, num_frames):
    """
    Resample the force vector to match the number of frames using interpolation.
    
    Parameters:
    force_values (numpy array): Original force values with a length of 15,000.
    num_frames (int): The total number of frames (6,000 in this case).
    
    Returns:
    resampled_force (numpy array): Resampled force values with a length of num_frames.
    """
    original_length = len(force_values)
    
    # Generate the indices corresponding to the original force data
    original_indices = np.linspace(0, original_length - 1, original_length)
    
    # Generate the indices corresponding to the new resampled data
    new_indices = np.linspace(0, original_length - 1, num_frames)
    
    # Create interpolation function (linear interpolation)
    interpolation_func = interp1d(original_indices, force_values, kind='linear')
    
    # Resample the force values
    resampled_force = interpolation_func(new_indices)
    
    return resampled_force

def calculate_correlations(distances, force_values, threshold):
    """
    Calculate correlations between distances and force values for pairs of residues that exceed the threshold.

    Parameters:
    distances (dictionary): Dictionary where keys are residue pairs and values are distance trajectories.
    force_values (numpy array): Array of shape (n_frames,) representing the force values.
    threshold (float): Threshold value to filter the pairs of residues based on the distance that seperates them (threshold is distnace).

    Returns:
    correlations (dictionary): Dictionary where keys are residue pairs that exceed the threshold and values are the correlations between distances and force values.
    """
    
  #  force_values= resample_force(force_values, len(distances))
    #init dict of correlations
    correlations = {}
    #loop over distances  dict  : pairs of residues are tuples and value are distances
    for pair, distance_trajectory in distances.items():
        distance_trajectory = np.array(distance_trajectory)  #  distance trajectory to NumPy array
       # print(distances[(1928, 1930)][0:20]) #distances between  1928 and 1930 from 1st to 20
        #discard distances that  do not respect threshold
        if np.any(distance_trajectory < threshold):
                        #vector of force that we extract from log contains all the data
            #we want the vectors to have the same size before correlation; 
            min_length = min(len(distance_trajectory), len(force_values))
            distance_trajectory = distance_trajectory[:min_length]
            force_values_residue = force_values[:min_length]
        
            #calculate correlations using np.corrcoef 
            # The function will return a symmetric matrix of correlation coefficients, 
            #with the diagonal elements being 1 (since each variable is perfectly correlated with itself).
            correlation = np.corrcoef(distance_trajectory, force_values_residue)[0, 1]     
            correlations[pair] = correlation
    return correlations


def launch_vmd(pdb_path, dcd_path):
    try:
        subprocess.run(["vmd", "-pdb", pdb_path, "-dcd", dcd_path])
        print("VMD launched successfully!")
    except FileNotFoundError:
        print("VMD executable not found. Make sure it is installed and in the system's PATH.")

def plot_correlation_heatmap(correlations):
    # Extract the residue indices from the correlations dictionary
    residues = sorted(list(set([residue for pair in correlations.keys() for residue in pair])))
    
    # Create an empty correlation matrix
    correlation_matrix = np.zeros((len(residues), len(residues)))
    
    # Fill the correlation matrix with the correlation coefficients
    for pair, correlation in correlations.items():
        residue1, residue2 = pair
        idx1 = residues.index(residue1)
        idx2 = residues.index(residue2)
        correlation_matrix[idx1, idx2] = correlation
        correlation_matrix[idx2, idx1] = correlation
    return correlation_matrix, residues

def plot_correlation_heatmap_multiple(correlations1, correlations2={}):
    # Extract the residue indices from the correlations dictionaries
    residues1 = sorted(list(set([residue for pair in correlations1.keys() for residue in pair])))
    residues2 = sorted(list(set([residue for pair in correlations2.keys() for residue in pair])))

    # Combine the residue indices from both matrices
    residues = sorted(list(set(residues1 + residues2)))

    # Create empty correlation matrix
    correlation_matrix = []

    # Fill the correlation matrix with the correlation coefficients
    for i, residue1 in enumerate(residues):
        row = []
        for j, residue2 in enumerate(residues):
            pair = (residue1, residue2)
            pair_symmetric = (residue2, residue1)

            if i == j:
                row.append(1.0)  # Set diagonal elements to 1.0
            elif i < j:
                if pair in correlations1:
                    row.append(correlations1[pair])
                elif pair in correlations2:
                    row.append(correlations2[pair])
                else:
                    row.append(0.0)
            else:
                # Fill the bottom side of the matrix with symmetric values
                if pair_symmetric in correlations1:
                    row.append(correlations1[pair_symmetric])
                elif pair_symmetric in correlations2:
                    row.append(correlations2[pair_symmetric])
                else:
                    row.append(0.0)
        correlation_matrix.append(row)

    # Convert correlation matrix to numpy array
    correlation_matrix = np.array(correlation_matrix, dtype=float)
    return correlation_matrix, residues




def calculate_work(pdb_path, dcd_path, force_vector, start_frame, end_frame, residue_ids):
    """
    Calculate pairwise distances (independently for x, y, z coordinates) between all residues,
    multiply each distance by the respective force component, and calculate the work over the trajectory.
    
    Parameters:
    pdb_path (str): Path to the PDB file.
    dcd_path (str): Path to the DCD file.
    force_vector (numpy array): Force vector (Fx, Fy, Fz) to multiply with distances.
    start_frame (int): The starting frame for analysis.
    end_frame (int): The ending frame for analysis.
    
    Returns:
    work_per_pair (dict): Dictionary where keys are pairs of residue IDs and values are the total work for each pair.
    """
    # load the trajectory and calculate the center of mass for each residue
    com_trajectories, residue_names = calculate_center_of_mass_trajectory(pdb_path, dcd_path, residue_ids)
    all_residue_ids = list(com_trajectories.keys())

    # initialize storage for force contribution calculation
    work_per_pair = {} 

    #loop through all pairs of residues
    for i in range(len(all_residue_ids)):
        for j in range(i + 1, len(all_residue_ids)):  # Only compute each pair once (i,j)
            residue1 = all_residue_ids[i]
            residue2 = all_residue_ids[j]

            # Get the center of mass trajectory of both residues
            com_trajectory1 = com_trajectories[residue1]
            com_trajectory2 = com_trajectories[residue2]

            # Initialize 
            total_work = 0.0

            # loop through frames and calculate distances and force contribution
            for frame_num in range(start_frame, end_frame):
                com1 = com_trajectory1[frame_num]
                com2 = com_trajectory2[frame_num]

                # Calculate the distance between the two residues for each coordinate
                dx = com1[0] - com2[0]  # Distance in x
                dy = com1[1] - com2[1]  # Distance in y
                dz = com1[2] - com2[2]  # Distance in z

                # Create the distance vector
                distance_vector = np.array([dx, dy, dz])
                
                # Calculate the work using dot product between distance vector and force vector
                work_frame = np.dot(distance_vector, force_vector)

                # Accumulate work over frames
                total_work += work_frame

            work_per_pair[(residue1, residue2)] = total_work


    return work_per_pair


def plot_work_heatmap(work_per_pair, residue_ids):
    """
    Plot a symmetric heatmap of force contributions between residue pairs.

    Parameters:
    force_contribution_per_pair (dict): Dictionary where keys are pairs of residue IDs and values are the force contributions.
    residue_ids (list): List of residue IDs to be used for labeling the heatmap.
    """
    # Initialize a square matrix for the heatmap
    n_residues = len(residue_ids)
    heatmap_data = np.zeros((n_residues, n_residues))

    # Fill the matrix with force contributions symmetrically
    for (residue1, residue2), contribution in work_per_pair.items():
        if residue1 in residue_ids and residue2 in residue_ids:
            idx1 = residue_ids.index(residue1)
            idx2 = residue_ids.index(residue2)
            # Assign the contribution to both (i, j) and (j, i) to ensure symmetry
            heatmap_data[idx1, idx2] = contribution
            heatmap_data[idx2, idx1] = contribution  # Make the matrix symmetric
    return heatmap_data


# import parse_SMD_log as smd
# import matplotlib.pyplot as plt
# import seaborn as sns
# import imageio.v2 as imageio

# log_file= "/home/mesbah/smd_software/inputs/mmCDH23_EC19-20/mmcdh23EC19-20_3CA_smd03a.log"
# pdb_path="/home/mesbah/smd_software/inputs/mmCDH23_EC19-20/mmcdh23EC19-20_3CA_wbi_noh20.pdb"
# dcd_path= "/home/mesbah/smd_software/inputs/mmCDH23_EC19-20/mmcdh23EC19-20_3CA_smd03a_noh20.dcd"
# residue_ids= list(range(1928, 2143))
# start_frame=0
# end_frame=30
# N=50
# threshold=13

# frames_list= list(range( start_frame, end_frame, N))
# # coeff_pairs_res= []

# # distances, residus = calculate_distances_between_residues(pdb_path, dcd_path, residue_ids, start_frame, end_frame)

# # df_eparms, df, smd_vel= smd.extract_info_from_smd_log (log_file)


# # distances, residus = calculate_distances_between_residues(pdb_path, dcd_path, residue_ids, start_frame, end_frame)
# # force_values=df["F[pN]"]
# query_residue=84
 

# force_vector = np.array([1.0, 0.0, 0.0])


# output_dir= "/media/mesbah/ADATA HD330/simulations_tcl/wt/I27/R2/smdc/"
# plot_work_heatmap(work_per_pair, residue_ids, output_dir)



# frames = []
# for index in np.arange(start_frame, end_frame, N):
#     frame_start = index
#     frame_end = index + N
#     print(f"Processing frames {frame_start} to {frame_end}")
#     work_per_pair = calculate_work(pdb_path, dcd_path, force_vector, frame_start, frame_end, residue_ids)
#     heatmap_data= plot_work_heatmap(work_per_pair, residue_ids)

#     plt.figure(figsize=(20,18))

#     # plt.imshow(heatmap_data, cmap='bwr', vmin=-10000, vmax=10000)
#     sns.heatmap(heatmap_data,  cmap='coolwarm', 
#                 xticklabels=residue_ids, yticklabels=residue_ids,
#                 cbar_kws={'label': 'work'})
#     plt.xlabel('Residues')
#     plt.ylabel('Residues')
#     plt.title(f"work Heatmap: Frames {frame_start} to {frame_end}")
#   #  plt.colorbar()
#     filename = f"{str(index).zfill(4)}.png"
#     plt.savefig(filename)
#     plt.close()
#     frames.append(filename)



# output_file_name = "/media/mesbah/ADATA HD330/simulations_tcl/wt/I27/R2/smdc/heatmap_work-very50.gif"
# with imageio.get_writer(output_file_name, mode='I', duration=end_frame) as writer:
#     for frame in frames:
#         image = imageio.imread(frame)
#         writer.append_data(image)



