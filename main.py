# -*- coding: utf-8 -*-
"""
Created on Wed May 27 17:40:13 2020

@author: Ismahene
"""
import os
import tkinter.messagebox  
from tkinter import * 
from tkinter.filedialog import askopenfilename 
from tkinter.filedialog import askopenfilenames 
from tkinter import filedialog
from scipy.ndimage import gaussian_filter
import scipy.signal as signal
from tkinter import ttk
from matplotlib.backends.backend_tkagg import ( FigureCanvasTkAgg,NavigationToolbar2Tk) 
import extensions as e
import functions as func
import webbrowser
from matplotlib.figure import Figure
import correlations as corr
import numpy as np
import pandas as pd
import matplotlib
import time
import imageio.v2 as imageio
matplotlib.use('TkAgg')

class App:
    def __init__(self):

        self.path=None
        self.df =None
        self.widget= None
        self.toolbar= None
        
        self.widget_main= None
        self.toolbar_main= None
        
        self.widget_tab2=None
        self.toolbar_tab2= None
        
        self.widget_2nd_detection= None
        self.toolbar_2nd_detection= None
        
        self.widget_slopes= None
        self.toolbar_slopes= None
        
        self.widget_refolding= None
        self.toolbar_refolding= None
        
        self.widget_correlations= None
        self.toolbar_correlations= None
        
        self.widget_work= None
        self.toolbar_work= None
        
        self.window = Tk() 
        self.style = ttk.Style(self.window)

        self.notebook = ttk.Notebook(self.window)
 
        self.tab1 = Frame(self.notebook,  width=600, height=200)
        self.tab2 = Frame(self.notebook,  width=600, height=200)
        self.tab3 = Frame(self.notebook,  width=600, height=200)
        self.tab4 = Frame(self.notebook,  width=600, height=200)
        self.tab5 = Frame(self.notebook,  width=600, height=200)
        self.tab6 = Frame(self.notebook,  width=600, height=200)
        self.tab7 = Frame(self.notebook,  width=600, height=200)
        self.tab8 = Frame(self.notebook,  width=600, height=200)

        self.notebook.add(self.tab1, text="Main")
        self.notebook.add(self.tab2, text="Peak detection")
        self.notebook.add(self.tab4, text="extension")
        self.notebook.add(self.tab5, text="peak detection from extension")
        self.notebook.add(self.tab6, text="Refolding detection")
        self.notebook.add(self.tab7, text="Correlations")
        self.notebook.add(self.tab8, text="work calculation")

        # initialization 
        self.frame=Frame(self.tab1)
        self.secndframe=Frame(self.tab2)
        self.thirdframe=Frame(self.tab3)
        self.fourthframe=Frame(self.tab4)
        self.fifthframe=Frame(self.tab5)
        self.sixframe=Frame(self.tab6)
        self.sevenframe=Frame(self.tab7)
        self.eightframe=Frame(self.tab8)

        self.notebook.pack(side='top')
        self.window.title("SMD software")
        self.window.minsize(600, 200)
        self.window.config()
# =============================================================================
#         columns
# =============================================================================
        #column names
        self.colnames=('time [s]', 'derivative at peak' , 'selection')
        self.columns=('peak index', 'Time [s]', 'Force [pN]', "LoadingRate[pN/s]", "Stiffness[pN/nm]" )
# =============================================================================
# subframes 1st tab
# =============================================================================
        self.subframe = Frame(self.frame)
        self.plot_frame_main=Frame(self.frame)
# =============================================================================
# subframes 2nd tab
# =============================================================================
        self.subframe_tab2 = Frame(self.secndframe)
        self.plot_frame_tab2=Frame(self.secndframe)
# =============================================================================
# subframes tab (extension)
# =============================================================================
        self.subframe_tab3= Frame(self.fourthframe)
        self.plot_frame_tab3= Frame(self.fourthframe)
# =============================================================================
# subframes slopes
# =============================================================================
        self.subframe_tab_slopes= Frame(self.thirdframe)
        self.plot_frame_slopes= Frame(self.thirdframe)
# =============================================================================
# subframes peak detection from the extension
# =============================================================================
        self.subframe_tab_2nd_detection= Frame(self.fifthframe)
        self.plot_frame_2nd_detection= Frame(self.fifthframe)
# =============================================================================
# subframes refolding detection
# =============================================================================
        self.subframe_tab_refolding= Frame(self.sixframe)
        self.plot_frame_refolding= Frame(self.sixframe)

# =============================================================================
# # subframes correlations of pairs of residues
# =============================================================================
        self.subframe_tab_correlation= Frame(self.sevenframe)
        self.plot_frame_correlation= Frame(self.sevenframe)
# =============================================================================
# # subframes work of pairs of residues
# =============================================================================
        self.subframe_tab_work= Frame(self.eightframe)
        self.plot_frame_work= Frame(self.eightframe)
# =============================================================================
# scollbars
# =============================================================================
        self.s=Scrollbar(self.subframe_tab_2nd_detection)
        self.scroll_bar=Scrollbar(self.subframe_tab2)
        self.scroll=Scrollbar(self.sixframe)
        # creation of composants
        self.create_widgets()
# =============================================================================
#         1st tab with two subframes : parameters and plots
# =============================================================================
        self.frame.pack(expand=YES)
        self.subframe.grid(row=0, column=1)
        self.plot_frame_main.grid(row=0, column=0)
# =============================================================================
#     2nd tab with two subframes: parameters, plots, table    
# =============================================================================  
        self.secndframe.pack(expand=YES)
        self.subframe_tab2.grid(row=0, column=1)
        self.plot_frame_tab2.grid(row=0, column=0)   
# =============================================================================
#         tab: extensions with 2 subframes 
# =============================================================================
        self.fourthframe.pack(expand=YES)
        self.subframe_tab3.grid(row=0, column=1)
        self.plot_frame_tab3.grid(row=0, column=0)
# =============================================================================
#     tab: slopes     
# =============================================================================
        self.thirdframe.pack(expand=YES)
        self.subframe_tab_slopes.grid(row=0, column=1)
        self.plot_frame_slopes.grid(row=0, column=0)
# =============================================================================
#   tab: 2 nd peak detection (from the extension)
# =============================================================================
        self.fifthframe.pack(expand=YES)
        self.subframe_tab_2nd_detection.grid(row=0, column=1)
        self.plot_frame_2nd_detection.grid(row=0, column=0)
# =============================================================================
#   tab refolding
# =============================================================================
        self.sixframe.pack(expand=YES)
        self.subframe_tab_refolding.grid(row=0, column=1)
        self.plot_frame_refolding.grid(row=0, column=0)
# =============================================================================
#         correlations
# =============================================================================
        self.sevenframe.pack(expand=YES)
        self.subframe_tab_correlation.grid(row=0, column=1)
        self.plot_frame_correlation.grid(row=0, column=0)
# =============================================================================
#work
# =============================================================================
        self.eightframe.pack(expand=YES)
        self.subframe_tab_work.grid(row=0, column=1)
        self.plot_frame_work.grid(row=0, column=0)
# ========================================================================
# =============================================================================
#     Treeview of 1st peak detection (tab2)
# =============================================================================
        #creation of treeview to  visualize detected peak detection 
        self.tab_peak_detection = ttk.Treeview(self.subframe_tab2,columns=self.columns,selectmode="extended",yscrollcommand=self.scroll_bar.set)
        self.tab_peak_detection.column("#0", width=0)
        #defenition of headers and size of columns of peak detection tab
        self.tab_peak_detection.heading("peak index", text="Index")
        self.tab_peak_detection.column("peak index",width=60)
        self.tab_peak_detection.heading("Time [s]", text="Time [s]") 
        self.tab_peak_detection.column("Time [s]",width=100, stretch=NO)
        # self.tab_peak_detection.heading("Time [s]", text="Time [s]") 
        # self.tab_peak_detection.column("Time [s]",width=80, stretch=NO)
        self.tab_peak_detection.heading("Force [pN]", text="Force [pN]")
        self.tab_peak_detection.column("Force [pN]",width=80, stretch=NO)
        self.tab_peak_detection.heading("LoadingRate[pN/s]", text="LoadingRate[pN/s]")
        self.tab_peak_detection.column("LoadingRate[pN/s]",width=100, stretch=NO)
        self.tab_peak_detection.heading("Stiffness[pN/nm]", text="Sitffness[pN/nm]")
        self.tab_peak_detection.column("Stiffness[pN/nm]",width=95, stretch=NO)
# =============================================================================
#    Treeview of 2nd peak detection (from the extension ) 
# =============================================================================
        #creation of treeview to  visualize detected unfolding peaks from the derivative of extension
        self.tab = ttk.Treeview(self.subframe_tab_2nd_detection,columns=self.colnames,selectmode="extended",yscrollcommand=self.s.set)
        self.tab.column("#0", width=0)
        self.tab.heading("time [s]", text="time [s]")
        self.tab.column("time [s]",minwidth=0,width=100, stretch=NO)
        self.tab.heading("selection", text="selection")
        self.tab.column("selection",minwidth=0,width=80, stretch=NO)
        self.tab.heading("derivative at peak", text="derivative at peak")
        self.tab.column("derivative at peak",minwidth=0,width=120, stretch=NO)
        self.s.config(command=self.tab.yview)
# =============================================================================
#   Treeview of refolding detection
# =============================================================================
        #creation of the treeview to vizualize the detected refolding peaks from the derivative of extension
        self.tab_refolding = ttk.Treeview(self.subframe_tab_refolding,columns=self.colnames,selectmode="extended",yscrollcommand=self.scroll.set)
        self.tab_refolding.column("#0", width=0)
        self.tab_refolding.heading("time [s]", text="time [s]")
        self.tab_refolding.column("time [s]",minwidth=0,width=200, stretch=NO)
        self.tab_refolding.heading("selection", text="selection")
        self.tab_refolding.column("selection",minwidth=0,width=200, stretch=NO)
        self.tab_refolding.heading("derivative at peak", text="derivative at peak")
        self.tab_refolding.column("derivative at peak",minwidth=0,width=200, stretch=NO)
        self.scroll.config(command=self.tab_refolding.yview)
# =============================================================================
# # widgets
# =============================================================================
    def create_widgets(self):
        "loads all widgets "
        self.create_buttons_maintab()
        self.create_buttons_tab2()
        self.create_buttons_tab3()
        self.create_buttons_slopes()
        self.create_buttons_detection_from_extension()
        self.create_buttons_refolding()
        self.create_buttons_correlation()
        self.create_buttons_work()
        
    def Close(self):
        if os.path.exists('all_logs.log'):
            os.remove('all_logs.log')
        
        if os.path.exists('energies.txt'):
            os.remove('energies.txt')
                
        
        if os.path.exists('log.txt'):
            os.remove('log.txt')
        
        if os.path.exists('all_dcds.dcd'):
            os.remove('all_dcds.dcd')
            
        self.window.destroy()
# =============================================================================
# 1st tab widgets and functions
# =============================================================================
    def fileDialog(self):
        """
        This function opens a window to select the log fle of SMD simulation
        Once the file selected force vs displacement (MainTab) 
        and force vs time (tab 2) are plotted 
        -------
        """
        path=askopenfilenames(initialdir =  "", title = "Select A File",
                                              filetypes =[("log file", "*.log"),("Text", "*.txt"),("CSV","*.csv")] ) 
        
        if len(path) >1:
            # Open file in write mode
            with open('all_logs.log', 'w') as all_logs:
                # Iterate through list
                for name in path:
                    # Open each file in read mode
                    with open(name) as infile:
                        # read the data from file1 and
                        # file2 and write it in file3
                        all_logs.write(infile.read())
                    # Add '\n' to enter data of file2
                    # from next line
                        all_logs.write("\n")
        if len(path)== 1:
            self.path= path[0]
            label_path=self.path
        elif len(path) != 1:
            self.path= "all_logs.log"
            label_file1= (path[0]).split( "/")[-1]
            label_file2= (path[1]).split( "/")[-1]
            common = os.path.commonprefix([label_file1, label_file2])

            label_path= str("merged_")+ str(len(path)) + common + str('_total') 
            

        print("Loading data from : ", str(path))
        self.pb1 = ttk.Progressbar(self.subframe, orient=HORIZONTAL, length=200,  mode='determinate')
        self.pb1.grid(row=2, columnspan=2, pady=20)
        for i in range(2):
            self.subframe.update_idletasks()
            self.pb1['value'] += 20
            time.sleep(1)
            
            
        label_path=Label(self.subframe, text=label_path,font=("Arial", 8),width=70,  bg='lightblue')
        label_path.grid(row=2, column=1)
       
        
        try:
            self.peaks.clear()
        except:
            self.peaks=[]
        self.FvsDis.grid(row=0,column=1)
        #plot force vs displacement
        self.plot_FvsDisplacement()
        knots=self.knots.get()
        #plot force vs time
        self.plot_FvsTime()
        self.compute_defaults_knots()

    def get_information(self):
        """
        This function exports the information
        of the log file into a csv file using
        tab separations

        Returns
        -------
        None.

        """
        export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
        self.df.to_csv(export_file_path, sep='\t',header=True, index=False, mode='w')
    
        
    def create_buttons_maintab(self):
        """
        creates all buttons of the first tab (main tab)

        Returns
        -------
        None.

        """
# =============================================================================
#         button to upload the log file
# =============================================================================
        upload_button = Button(self.subframe, text="1- Upload log file or a Tab-Separated file (csv or txt)", font=("Courrier", 10)
                               ,width=60, command=self.fileDialog)
        upload_button.grid(row=1,column=1)
        
        
        "creates uploading button for pdb file "
        upload_pdb_button = Button(self.subframe, text="2- UPLOAD PDB FILE", font=("Courrier", 10), width=60, command=self.filePDB) 
        upload_pdb_button.grid(row=3,column=1)   
        

        "creates button of dcd file"
        self.dcd_button = Button(self.subframe, text="3- UPLOAD DCD  FILE", font=("Courrier", 10)
                          ,width=60, command=self.fileDCD)
        
        
        
        self.link_tab_file = Label(self.subframe, text="Tab-Sep file", font=("Courrier", 10), bg='lightblue', cursor="hand2") 

        self.link_tab_file.bind("<Enter>", self.on_enter_csv)
        self.link_tab_file.bind("<Leave>", self.on_leave_csv)
        self.link_tab_file.grid(row=1, column=2)
# =============================================================================
# button to export information of log file to csv
# =============================================================================
        self.export_info_b= Button(self.subframe, text="5- export information to file",
                       font=("Courrier", 10),width=30,command= self.get_information)
# =============================================================================
# button   :plot force vs displacement 
# =============================================================================
        self.FvsDis = Button(self.subframe, text=" plot force vs displacement",
                        font=("Courrier", 10),width=40, command=self.plot_FvsDisplacement)
# =============================================================================
# button: plot energy parameters
# =============================================================================
        self.eparms_button= Button(self.subframe, text="4- plot energy parameters" ,
                        font=("Courrier", 10),width=30,command=self.plot_energyVStimeStep)
# =============================================================================
# Checkbuttons of energy parms
# =============================================================================
        self.radiobutton_eparms = StringVar()
        self.radiobutton_g = IntVar()
        self.radiobutton_tcl_force= IntVar()

        self.radiobutton_bond= Radiobutton(self.subframe, text="BOND", variable= self.radiobutton_eparms, value= "BOND",  width=10)
        self.radiobutton_angle= Radiobutton(self.subframe, text="ANGLE", variable= self.radiobutton_eparms, value= "ANGLE",  width=10)
        self.radiobutton_dihed= Radiobutton(self.subframe, text="DIHED", variable= self.radiobutton_eparms, value= "DIHED" ,width=10)
        self.radiobutton_imprp= Radiobutton(self.subframe, text="IMPRP", variable= self.radiobutton_eparms, value= "IMPRP",  width=10)
        self.radiobutton_elect= Radiobutton(self.subframe, text="ELECT", variable= self.radiobutton_eparms, value= "ELECT",  width=10)
        self.radiobutton_VDW= Radiobutton(self.subframe, text="VDW", variable= self.radiobutton_eparms, value= "VDW",  width=10)
        # boudary energy is from spherical boundary conditions and harmonic restraints
        self.radiobutton_bound= Radiobutton(self.subframe, text="BOUNDARY", variable= self.radiobutton_eparms, value= "BOUNDARY", width=10)
        # MISC energy is from external electric fields and various steering forces
        self.radiobutton_misc= Radiobutton(self.subframe, text="MISC", variable= self.radiobutton_eparms, value= "MISC",  width=10)
        self.radiobutton_kinetic= Radiobutton(self.subframe, text="KINETIC", variable= self.radiobutton_eparms, value= "KINETIC", width=10)
        #TOTAL is the sum of the various potential energies, and the KINETIC energy
        self.radiobutton_total= Radiobutton(self.subframe, text="TOTAL", variable= self.radiobutton_eparms, value= "TOTAL", width=10)
        self.radiobutton_temp= Radiobutton(self.subframe, text="TEMP", variable= self.radiobutton_eparms, value= "TEMP", width=10)
        self.radiobutton_potential= Radiobutton(self.subframe, text="POTENTIAL", variable= self.radiobutton_eparms, value= "POTENTIAL", width=10)
        # TOTAL2 uses a slightly different kinetic energy that is better conserved during equilibration in a constant
        # energy ensemble. TOTAL3 is another variation with much smaller short-time fluctuations that is also adjusted to have the same running average as TOTAL2. Defects in
        # constant energy simulations are much easier to spot in TOTAL3 than in TOTAL or
        # TOTAL2.
        self.radiobutton_total3= Radiobutton(self.subframe, text="TOTAL3", variable= self.radiobutton_eparms, value= "TOTAL3", width=10)
        self.radiobutton_tempavg= Radiobutton(self.subframe, text="TEMPAVG", variable= self.radiobutton_eparms, value= "TEMPAVG", width=10)
       # PRESSURE is the pressure calculated based on individual atoms
        self.radiobutton_pressure= Radiobutton(self.subframe, text="PRESSURE", variable= self.radiobutton_eparms, value= "PRESSURE", width=10)
        # GPRESSURE incorporates hydrogen atoms into the heavier atoms to which they are bonded, producing smaller fluctuations
        self.radiobutton_gpressure= Radiobutton(self.subframe, text="GPRESSURE", variable= self.radiobutton_eparms, value= "GPRESSURE", width=10)
        self.radiobutton_volume= Radiobutton(self.subframe, text="VOLUME", variable= self.radiobutton_eparms, value= "VOLUME", width=10)
        
        # The TEMPAVG, PRESSAVG, and GPRESSAVG are the average of
        # temperature and pressure values since the previous ENERGY output; for the first step
        # in the simulation they will be identical to TEMP, PRESSURE, and GPRESSURE.
        self.radiobutton_pressavg= Radiobutton(self.subframe, text="PRESSAVG", variable= self.radiobutton_eparms, value= "PRESSAVG", width=10)
        self.radiobutton_gpressavg= Radiobutton(self.subframe, text="GPRESSAVG", variable= self.radiobutton_eparms, value= "GPRESSAVG", width=10)
        
        
        self.radiobutton_grid = ttk.Checkbutton(self.subframe,  text='Add Grid',variable= self.radiobutton_g, onvalue=1, offvalue=0,  width=10)
        self.radiobutton_tcl_force_button= ttk.Checkbutton(self.subframe,  text='See F(-v)',variable= self.radiobutton_tcl_force, onvalue=1, offvalue=0,  width=10)


        exit_button_correlation_tab= Button(self.subframe_tab_correlation, text='EXIT', command=self.Close)
        exit_button_correlation_tab.grid(row=0, column=3)
        
        exit_button_1st_tab= Button(self.subframe, text='EXIT', command=self.Close)
        exit_button_1st_tab.grid(row=0, column=3)
        
        exit_button_2nd_tab= Button(self.subframe_tab2, text='EXIT', command=self.Close)
        exit_button_2nd_tab.grid(row=0, column=3)
        
        exit_button_3rd_tab= Button(self.subframe_tab3, text='EXIT', command=self.Close)
        exit_button_3rd_tab.grid(row=0, column=3)
        
        exit_button_tab_slopes= Button(self.subframe_tab_slopes, text='EXIT', command=self.Close)
        exit_button_tab_slopes.grid(row=0, column=3)
        
        exit_button_2nd_detection_tab= Button(self.subframe_tab_2nd_detection, text='EXIT', command=self.Close)
        exit_button_2nd_detection_tab.grid(row=0, column=3)
        
        exit_button_refolding_tab= Button(self.subframe_tab_refolding, text='EXIT', command=self.Close)
        exit_button_refolding_tab.grid(row=0, column=3)
        
        exit_button_work_tab= Button(self.subframe_tab_work, text='EXIT', command=self.Close)
        exit_button_work_tab.grid(row=0, column=3)

    def plot_FvsDisplacement(self):
        """
        This function plot force vs displacement
        After the 'plot force vs displacement' button 
        is pressed, radiobuttons of energy parameters appear

        Returns
        -------
        None.

        """
        start=time.time()
        # remove old widgets
        if self.widget_main:
            self.widget_main.destroy()

        if self.toolbar_main:
            self.toolbar_main.destroy()
            
        figure = Figure(figsize=(6,5), dpi=100)
        ax=figure.add_subplot(111)
        ax.set_xlabel('Displacement [nm]')
        ax.set_ylabel('Force [pN]')
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_main)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=10, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_main)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
      #  figure.tight_layout()
        self.toolbar_main.update()
        if self.radiobutton_g.get() ==1:
            ax.grid()

        if  os.path.splitext(self.path)[-1].lower() =='.csv' or  os.path.splitext(self.path)[-1].lower() == '.txt':
            self.df= func.read_data_csv(self.path)

        else: 
            self.df_energy_parms, self.df, self.velocity= func.extract_info_from_smd_log(self.path)
            self.force_vector= np.array(self.df['direction'][:3])
        
        self.pb1.destroy()
        
        try:
            self.df['-F[pN]']
            if self.radiobutton_tcl_force.get() ==1:
                ax.plot(self.df['displacement[nm]'], self.df['F[pN]'], label="SMD force")
                ax.plot(self.df['displacement[nm]'], self.df['-F[pN]'],label="TCL force", color='red')
                ax.legend()
            else:
                ax.plot(self.df['displacement[nm]'], self.df['F[pN]'])
        except:
            ax.plot(self.df['displacement[nm]'], self.df['F[pN]'])
        
        #1st column
        self.eparms_button.grid(row=15,column=1)
        self.radiobutton_bond.grid(row=6, column=0)
        self.radiobutton_angle.grid(row=7, column=0)
        self.radiobutton_dihed.grid(row=8, column=0)
        self.radiobutton_imprp.grid(row=9, column=0)
        self.radiobutton_misc.grid(row=10, column=0)
        self.radiobutton_pressavg.grid(row=11, column=0)
        self.radiobutton_bound.grid(row=12, column=0)

        # 3rd column
        self.radiobutton_total3.grid(row=6, column=2)
        self.radiobutton_elect.grid(row=7, column=2)
        self.radiobutton_VDW.grid(row=8, column=2)
        self.radiobutton_tempavg.grid(row=9, column=2)
        self.radiobutton_pressure.grid(row=10, column=2)
        self.radiobutton_gpressure.grid(row=11, column=2)
        # 2nd column
        self.radiobutton_kinetic.grid(row=7, column=1)
        self.radiobutton_total.grid(row=8, column=1)
        self.radiobutton_temp.grid(row=9, column=1)
        self.radiobutton_potential.grid(row=10, column=1)
        self.radiobutton_volume.grid(row=11, column=1)
        self.radiobutton_gpressavg.grid(row=12, column=1)
        self.radiobutton_grid.grid(row=2 , column=2)
        self.export_info_b.grid(row=20,column=1)  

        self.radiobutton_tcl_force_button.grid(row= 3, column =2)

        end=time.time()
        print('plot force vs displacement ', round(end-start, 2), ' seconds')
        
    def plot_energyVStimeStep(self):
        """
        This function plot an energy parameters
        selected by the user from the radiobutton once
        the button is pressed

        Returns
        -------
        None.

        """
        start=time.time()
        # remove old widgets
        if self.widget_main:
            self.widget_main.destroy()

        if self.toolbar_main:
            self.toolbar_main.destroy()
            
        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        ax.set_xlabel("time in seconds")
        canvas= FigureCanvasTkAgg(figure, self.plot_frame_main)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=10, column=1)

        toolbarFrame = Frame(master= self.plot_frame_main)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_main)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                   toolbarFrame) 
        self.toolbar_main.update()
        if self.radiobutton_g.get() ==1:
            ax.grid()

        if self.radiobutton_eparms.get()=="BOND":
           # df_energy_parms,self.df=extract_info_from_smd_log(self.path)
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['BOND'])
            ax.set_ylabel("BOND")
                   
        elif self.radiobutton_eparms.get()=="ANGLE":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['ANGLE'])
            ax.set_ylabel("ANGLE")

        elif self.radiobutton_eparms.get()=="DIHED":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['DIHED'])
            ax.set_ylabel("DIHED")

        elif self.radiobutton_eparms.get()=="IMPRP":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['IMPRP'])
            ax.set_ylabel("IMPRP")

        elif self.radiobutton_eparms.get()=="ELECT":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['ELECT'])
            ax.set_ylabel("ELECT")
                        
        elif self.radiobutton_eparms.get()=="VDW":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['VDW'])
            ax.set_ylabel("VDW")

        elif self.radiobutton_eparms.get()=="BOUNDARY":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['BOUNDARY'])
            ax.set_ylabel("BOUNDARY")
        
        elif self.radiobutton_eparms.get()=="MISC":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['MISC'])
            ax.set_ylabel("MISC")
        
        elif self.radiobutton_eparms.get()=="KINETIC":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['KINETIC'])
            ax.set_ylabel("KINETIC")
        
        elif self.radiobutton_eparms.get()=="TOTAL":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['TOTAL'])
            ax.set_ylabel("TOTAL")
        
        elif self.radiobutton_eparms.get()=="TEMP":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['TEMP'])
            ax.set_ylabel("TEMP")

        elif self.radiobutton_eparms.get()=="POTENTIAL":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['POTENTIAL'])
            ax.set_ylabel("POTENTIAL")

        elif self.radiobutton_eparms.get()=="TOTAL3":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['TOTAL3'])
            ax.set_ylabel("TOTAL3")

        elif self.radiobutton_eparms.get()=="TEMPAVG":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['TEMPAVG'])
            ax.set_ylabel("TEMPAVG")

        elif self.radiobutton_eparms.get()=="PRESSURE":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['PRESSURE'])
            ax.set_ylabel("PRESSURE")

        elif self.radiobutton_eparms.get()=="GPRESSURE":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['GPRESSURE'])
            ax.set_ylabel("GPRESSURE")

        elif self.radiobutton_eparms.get()=="VOLUME":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['VOLUME'])
            ax.set_ylabel("VOLUME")

        elif self.radiobutton_eparms.get()=="PRESSAVG":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['PRESSAVG'])
            ax.set_ylabel("PRESSAVG")

        elif self.radiobutton_eparms.get()=="GPRESSAVG":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['GPRESSAVG'])
            ax.set_ylabel("GPRESSAVG")

        elif not  self.radiobutton_eparms.get():
            tkinter.messagebox.showinfo("Warning ", "Please select an energy parameter")
        
        self.radiobutton_g.set(0)

        end=time.time()
        print('plot energy parameters ', round(end-start, 2), ' seconds')
# =============================================================================
# 2nd tab buttons and functions
# =============================================================================
    def plot_FvsTime(self):
        """
        This function plots force vs time curve
        once the button is pressed, knots and entries appear
        the 'detect peaks' button also appears
        
        Returns
        -------
        None.

        """
        start=time.time()
        if self.path==None :
            tkinter.messagebox.showinfo("Warning ", "Please upload the log file ")
                # remove old widgets
        if self.widget_tab2:
            self.widget_tab2.destroy()

        if self.toolbar_tab2:
            self.toolbar_tab2.destroy()
        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_tab2)
        canvas.draw()
        self.widget_tab2= canvas.get_tk_widget()
        self.widget_tab2.grid(row=10, column=1)
                # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_tab2)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_tab2 = NavigationToolbar2Tk(canvas, toolbarFrame)
        ax.set_xlabel('time [s]')
        ax.set_ylabel('Force [pN]')
        if self.radiobutton_g_tab2.get()==1:
            ax.grid()
            
        if self.df is None:
            self.energy_parms, self.df= func.extract_info_from_smd_log(self.path)
                                   
        elif not self.knots.get() and self.df is not None:
            ax.plot(self.df['timeInS'], self.df['F[pN]'])

        elif self.knots.get() and self.df is not None:
            model=func.get_natural_cubic_spline_model(self.df['timeInS'], self.df['F[pN]'], minval=min(self.df['timeInS']), maxval=max(self.df['timeInS']), n_knots=self.knots.get())
            smooth=model.predict(self.df['timeInS'])
            ax.plot(self.df['timeInS'], self.df['F[pN]'])
            ax.plot(self.df['timeInS'], smooth)
        
        #if file contains TCL forces
        try:
            self.df['-F[pN]']
            #if the user choosed to see these TCL forces
            if self.force_tcl_tab2.get() ==1 : 
                ax.plot(self.df['timeInS'], self.df['-F[pN]'], color='red')
                #make sure the dataframe is already loaded
                if self.df is None:
                    self.energy_parms, self.df, self.velocity= func.extract_info_from_smd_log(self.path)

                #if there is NO knot value, just df values
                elif not self.knots.get() and self.df is not None:
                    ax.plot(self.df['timeInS'], self.df['-F[pN]'], color='red')
                # if there is knot value : smoothing
                elif self.knots.get() and self.df is not None:
                    model=func.get_natural_cubic_spline_model(self.df['timeInS'], self.df['-F[pN]'], minval=min(self.df['timeInS']), maxval=max(self.df['timeInS']), n_knots=self.knots.get())
                    smooth=model.predict(self.df['timeInS'])
                    ax.plot(self.df['timeInS'], self.df['-F[pN]'],  color='red')
                    ax.plot(self.df['timeInS'], smooth)
        #if the file does not contain TCL forces: untick the checkbutton    
        except:
            self.force_tcl_tab2.set(0) 

        self.label.grid(row=1,column=1) #give smoothing value
        self.label1.grid(row=2,column=1) #click for more details
        self.entry_knots.grid(row=3,column=1)
        self.knots_button.grid(row=4, column=1) # button: use default value of knots
        self.knots_infos.grid(row=5, column=1) #info knots
        self.prominence_entry.grid(row=7,column=1)
        self.prominence_b.grid(row=8,column=1) # button: see prominence distribution
        self.reset.grid(row=11, column=1)
        self.label_prominence.grid(row=6, column=1)
        self.link1.grid(row=7,column=1, sticky=E) #link prominence ('?')
        self.peaks_button.grid(row=9,column=1) 
        
        self.radiobutton_grid_tab2.grid(row=0, column=2)
        self.f_tcl_tab2.grid(row=3, column=2)
        
        self.link2.grid(row=11 ,column=1, sticky=E) #link grid('?')
 
        end=time.time()
        print('plot force vs time ', round(end-start, 2), ' seconds')

    def create_buttons_tab2(self):
        """
        creates all the buttons of the 2nd tab

        Returns
        -------
        None.

        """
        self.radiobutton_g_tab2= IntVar()
        self.force_tcl_tab2= IntVar()
        
# =============================================================================
# button force vs time in seconds
# =============================================================================
        FvsT = Button(self.subframe_tab2, text="1- plot force vs time",
                      width=30,command=self.plot_FvsTime)
        FvsT.grid(row=0,column=1)

        self.radiobutton_grid_tab2 = ttk.Checkbutton(self.subframe_tab2,  text='Add Grid',variable= self.radiobutton_g_tab2, onvalue=1, offvalue=0,  width=10)
        self.f_tcl_tab2 = ttk.Checkbutton(self.subframe_tab2,  text='Plot F(-v)',variable= self.force_tcl_tab2, onvalue=1, offvalue=0,  width=10)

        #save selected peaks
        self.save_peaks = Button(self.subframe_tab2, text="4- Save selected peaks",
                      width=30, command=self.save_selected_peaks_1st_detection)
        self.export_data_1st_detection=Button(self.subframe_tab2, text= "export data", 
                                              width=30, command=self.export_slopes)

# =============================================================================
# prominence button
# =============================================================================
        self.prominence_b = Button(self.subframe_tab2, text="See prominence distribution",
                      width=30,command=self.plot_prominences)

# =============================================================================
# links
# =============================================================================
        self.link1 = Label(self.subframe_tab2, text="?",  bg='lightblue', cursor="hand2") 
        self.link2 = Label(self.subframe_tab2, text="?",  bg='lightblue', cursor="hand2") 

        self.link1.bind("<Enter>", self.on_enter)
        self.link1.bind("<Leave>", self.on_leave)
        self.link1.bind("<Button-1>", lambda e: self.callback("https://en.wikipedia.org/wiki/Topographic_prominence"))
# =============================================================================
#   defaults knots buttons
# =============================================================================
        self.knots_button=Button(self.subframe_tab2, text='2- Use default value' + '\n'+'to smooth data (knots)', width=30,font=("Courrier", 10),
                            command=self.compute_defaults_knots)
        self.knots_infos=Button(self.subframe_tab2, text='Information about knots computing ', 
                           width=30, command=self.information_knots)

        self.knots=IntVar()
        self.label1=Label(self.subframe_tab2, text="Click for details", bg='lightblue', cursor="hand2")
        self.label=Label(self.subframe_tab2, text="Give value (knots)"+ '\n'+" to smooth data (starting from 2)")

        self.label1.bind("<Button-1>", lambda e: self.callback("https://towardsdatascience.com/numerical-interpolation-natural-cubic-spline-52c1157b98ac"))
        self.entry_knots=Entry(self.subframe_tab2, textvariable=self.knots, width=30)
        
        self.reset=Button(self.subframe_tab2, text="Reset",  width=30,command=self.clear_button)
        
        self.prominence =IntVar()

        self.label_prominence=Label(self.subframe_tab2, text= 'Prominence',  width=30)
        self.prominence_entry= Entry(self.subframe_tab2, textvariable=self.prominence,  width=30)
        
        self.peaks_button = Button(self.subframe_tab2, text= "3- Detect peaks", width=30,command=self.plot_peaks_detection)
        
        self.show_saved_peaks = Button(self.subframe_tab2, text= "Show saved peaks",  width=30,command=self.list_total_peaks_1st_detection)
        
        self.delete_this_peak_button = Button(self.subframe_tab2, text= "unsave peak", width=30,command=self.delete_from_total_list_1st_detection)

    def clear_button(self):
        """
        This function is called when the user
        presses the 'Reset' button of the 2nd tab
        It clears the knots entry
        """
        self.knots.set(0)

    def callback(self, url):
        """
        opens a web page 
        Parameters
        ----------
        url :string
            An url for more details.
        """
        webbrowser.open_new(url)
        
    def on_enter_csv(self, event):
        """
        Gives a definition of prominence when the user hovers
        Parameters
        ----------
        event : string
            <Enter>.
        """
        self.link_tab_file.configure(text="Header must be:" +'\n'+"timeStep, timeInS[sec], displacement[nm], "+'\n'+"F[pN], -F[pN]", bg='white')
    
    def on_enter(self, event):
        """
        Gives a definition of prominence when the user hovers
        Parameters
        ----------
        event : string
            <Enter>.
        """
        self.link1.configure(text="The prominence of a peak  measures"+'\n'+" how much the peak stands out to other peaks."+'\n'" Click to know more", bg='white', font='Courrier')

    def on_leave(self, enter):
        """
        when the user leaves the label becomes ? (like in the initial state)
        Parameters
        ----------
        enter : string
            on leave.
        """
        self.link1.configure(text="?", bg='lightblue')
        
    def on_leave_csv(self, enter):
        """
        when the user leaves the label becomes ? (like in the initial state)
        Parameters
        ----------
        enter : string
            on leave.
        """
        self.link_tab_file.configure(text="?", bg='lightblue')
      
    def on_leave_link2(self, enter):
        """
        When the user leaves the ? is shown again
        Parameters
        ----------
        enter : str
            the action of leaving to this label.
        """
        self.link2.configure(text="?", bg='lightblue')
        
    def information_knots(self):
        tkinter.messagebox.showinfo("information ", "Knots value is computed as follows: we take a moving window width of 0.3 nm then we devide the full length* in nm by 0.4."+ '\n' + "*Full length is the total displacement.")
   
# =============================================================================
# 1 st peak detection: save peaks
# =============================================================================
    def save_selected_peaks_1st_detection(self):
        """
        This function saves the peaks that the user selects: 1st tab

        """
        try:
            self.peaks
        except:
            self.peaks=[]
        else:
            timeInS= list(self.df['timeInS'])
            for i in range(len(timeInS)):
                timeInS[i]= "{:.6e}".format(timeInS[i])
            curItem= self.tab_peak_detection.selection()
            curItems = [(self.tab_peak_detection.item(i)['values']) for i in curItem]
            user_selection=[]
            self.selection=[]
            for i in curItems:
                user_selection.append(float(i[1]))
                print(user_selection)

                self.selection.append(i[-1])
                print(self.selection)

            for i in range(len(user_selection)):
                
                user_selection[i]= "{:.6e}".format(user_selection[i])
                if timeInS.index(user_selection[i]) not in self.peaks:
                    self.peaks.append(timeInS.index(user_selection[i]))     
                
            tkinter.messagebox.showinfo("informations ", "Added to list successfully")


    def plot_prominences(self):
        """
        plots the distribution of prominences
        """

        start=time.time()
        # remove old widgets
        if self.widget_tab2:
            self.widget_tab2.destroy()

        if self.toolbar_tab2:
            self.toolbar_tab2.destroy()

        figure = Figure(figsize=(7,5), dpi=100)
        ax= figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_tab2)
        canvas.draw()
        self.widget_tab2=canvas.get_tk_widget()
        self.widget_tab2.grid(row=10, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_tab2)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_tab2 = NavigationToolbar2Tk(canvas, 
                                   toolbarFrame) 
        self.toolbar_tab2.update()
        ax.set_title('Prominence distribution')
        
        if self.radiobutton_g_tab2.get() ==1:
            ax.grid()
        try:
            self.df['-F[pN]']
            if self.force_tcl_tab2.get() == 1:
                if self.knots.get():
                    knots=self.knots.get()
                    prominences=func.find_peaks(self.df['F[pN]'], self.df['timeInS'],  knots)[2]
                    peaks=func.find_peaks(self.df['F[pN]'], self.df['timeInS'],  knots)[0]
                    prominences = signal.peak_prominences(x, peaks)[0]

                    ax.plot(  self.df['displacement[nm]'][peaks], prominences)
                    prominences=func.find_peaks(self.df['-F[pN]'], self.df['timeInS'],  knots)[2]

                else:
                    peaks=signal.find_peaks(self.df['-F[pN]'])[0]
                    prominences = signal.peak_prominences(self.df['-F[pN]'], peaks)[0]
                    ax.bar(range(len(prominences)), prominences)

            else:
                if self.knots.get():
                    knots=self.knots.get()
                    prominences=func.find_peaks(self.df['F[pN]'], self.df['timeInS'],  knots)[2]
                    ax.bar(range(len(prominences)), prominences)

                else:
                    peaks=signal.find_peaks(self.df['F[pN]'])[0]
                    prominences = signal.peak_prominences(self.df['F[pN]'], peaks)[0]
                    ax.bar(range(len(prominences)), prominences)

        except:
            if self.knots.get():
                knots=self.knots.get()
                prominences=func.find_peaks(self.df['F[pN]'], self.df['timeInS'],  knots)[2]
                ax.bar(range(len(prominences)), prominences)

            else:
                peaks=signal.find_peaks(self.df['F[pN]'])[0]
                prominences = signal.peak_prominences(self.df['F[pN]'], peaks)[0]
                ax.bar(range(len(prominences)), prominences)

        self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('plot prominence ', round(end-start,2) , ' seconds')
  
    def compute_defaults_knots(self):
        """
        Computes the default value of knots and writes
        it on the right entry

        Returns
        -------
        None.

        """
        start= time.time()

        f=self.df['F[pN]']
        displacement=np.array(self.df['displacement[nm]'])
        displacement=float(displacement[-1])
        knots=int( displacement  // 0.3 ) 

        if knots <=20:
            knots=50
        peaks=signal.find_peaks(f)[0]
        prominences=signal.peak_prominences(f, peaks)[0]
        default_prominence=int(np.mean(prominences))
        self.knots.set(knots)
        self.prominence.set(default_prominence)
        end= time.time()
        print('knots computing ', round(end-start,2) , ' seconds')

# =============================================================================
#     1st peak detection:    plot peaks
# =============================================================================
    def plot_peaks_detection(self):
        """
        plots peaks on force vs time curve

        Returns
        -------
        None.

        """
        start=time.time()
        if self.widget_tab2:
            self.widget_tab2.destroy()

        if self.toolbar_tab2:
            self.toolbar_tab2.destroy()
            
        figure = Figure(figsize=(7,5), dpi=100)
        ax= figure.add_subplot(111)
        ax.set_xlabel('time [s]')
        ax.set_ylabel('Force [pN]')
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_tab2)
        canvas.draw()
        self.widget_tab2= canvas.get_tk_widget()
        self.widget_tab2.grid(row=10, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_tab2)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_tab2 = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        self.toolbar_tab2.update()
        if self.radiobutton_g_tab2.get()==1:
            ax.grid()
        p=self.prominence.get()
        distance_slope=self.distance.get()
        knots= self.knots.get()
# =============================================================================
#             if tcl forces recorded and users ticks the box
# =============================================================================
        if self.force_tcl_tab2.get() == 1:
            try:
                self.df['-F[pN]']
            except:
                print('No tcl forces recorded during the simulation, untick F(-v)')
                tkinter.messagebox.showinfo("Warning ", "No tcl forces recoded during simulation, untick F(-v) on the right side")

# =============================================================================
#                 if any knot number entered for -F
# =============================================================================
            if knots:
                smooth,peaks, prominences=func.find_peaks(self.df['-F[pN]'], self.df['timeInS'], knots=knots,prominence=p)
                ax.plot(self.df['timeInS'],self.df['-F[pN]'])
                ax.plot(self.df['timeInS'],smooth)
                ax.plot(self.df['timeInS'][peaks], smooth[peaks], "or")
    
                self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                d, slope, intercept = func.get_slopes(self.df['displacement[nm]'], self.df['timeInS'], self.df['-F[pN]'], peaks, distance_slope )
                    

                for i in range(len(peaks)):

                    self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.6e}".format(self.df['timeInS'][peaks[i]]),"{:.2e}".format(self.df['-F[pN]'][peaks[i]]), "{:.2e}".format(slope[i]), "{:.2e}".format(slope[i]/ (self.velocity*10**9))))

                for col in self.columns:
                    self.tab_peak_detection.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab_peak_detection, _col, False))
                    
                self.tab_peak_detection.grid(row=20, column=1)   
                self.scroll_bar.grid(row=20, column=2, sticky="ns")
                self.save_peaks.grid(row=25,column=1)
                self.export_data_1st_detection.grid(row=30, column=1)
                self.show_saved_peaks.grid(row=28, column=1)
                self.delete_this_peak_button.grid(row=29, column=1)
            else:
# =============================================================================
#                     if no knots entered for -F
# =============================================================================
                peaks=signal.find_peaks(self.df['-F[pN]'], prominence=p)[0]
                ax.plot(self.df['timeInS'], self.df['-F[pN]'])
                ax.plot(self.df['timeInS'][peaks],self.df['-F[pN]'][peaks], "or")
                self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                

                d, slope, intercept = func.get_slopes(self.df['displacement[nm]'], self.df['timeInS'], self.df['-F[pN]'], peaks, distance_slope )
                
                for i in range(len(peaks)):
                    self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.6e}".format(self.df['timeInS'][peaks[i]]), "{:.2e}".format(self.df['-F[pN]'][peaks[i]]), "{:.2e}".format(slope[i] ), "{:.2e}".format(slope[i]/ (self.velocity*10**9))))
                
                for col in self.columns:
                    self.tab_peak_detection.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab_peak_detection, _col, False))
                self.tab_peak_detection.grid(row=20, column=1)   
                self.scroll_bar.grid(row=20, column=2, sticky="ns")
                self.save_peaks.grid(row=25,column=1)
                self.export_data_1st_detection.grid(row=30, column=1)
                self.show_saved_peaks.grid(row=28, column=1)
                self.delete_this_peak_button.grid(row=29, column=1)

# =============================================================================
#               else: if users did not tick -F (tcl forces not avaialble)
# =============================================================================
        else:
            if knots:
# =============================================================================
#                     # if knots entered for F
# =============================================================================
                smooth,peaks, prominences=func.find_peaks(self.df['F[pN]'], self.df['timeInS'], knots=knots,prominence=p)
                ax.plot(self.df['timeInS'],self.df['F[pN]'])
                ax.plot(self.df['timeInS'],smooth)
                ax.plot(self.df['timeInS'][peaks], smooth[peaks], "or")
    
                self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                

                d, slope, intercept = func.get_slopes(self.df['displacement[nm]'], self.df['timeInS'], self.df['F[pN]'], peaks,  distance_slope )
                    
                for i in range(len(peaks)):  
                    self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.6e}".format(self.df['timeInS'][peaks[i]]), "{:.2e}".format(self.df['F[pN]'][peaks[i]] ), "{:.2e}".format(slope[i]), "{:.2e}".format(slope[i]/ (self.velocity*10**9))))
                
                for col in self.columns:
                    self.tab_peak_detection.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab_peak_detection, _col, False))
                
                self.tab_peak_detection.grid(row=20, column=1)   
                self.scroll_bar.grid(row=20, column=1, sticky="ns")
                self.save_peaks.grid(row=25,column=1)
                self.export_data_1st_detection.grid(row=30, column=1)
                self.show_saved_peaks.grid(row=28, column=1)
                self.delete_this_peak_button.grid(row=29, column=1)

            else:
# =============================================================================
#                     if no knots suggested for F
# =============================================================================
                peaks=signal.find_peaks(self.df['F[pN]'], prominence=p)[0]
                ax.plot(self.df['timeInS'], self.df['F[pN]'])
                ax.plot(self.df['timeInS'][peaks],self.df['F[pN]'][peaks], "or")
                self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                

                d, slope, intercept = func.get_slopes(self.df['displacement[nm]'], self.df['timeInS'], self.df['F[pN]'], peaks,  distance_slope )
                
                for i in range(len(peaks)):
                    self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.6e}".format(self.df['timeInS'][peaks[i]]), "{:.2e}".format(self.df['F[pN]'][peaks[i]]) , "{:.2e}".format(slope[i]), "{:.2e}".format(slope[i]/ (self.velocity*10**9))))
                
                for col in self.columns:
                    self.tab_peak_detection.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab_peak_detection, _col, False))
                
                self.tab_peak_detection.grid(row=20, column=1)   
                self.scroll_bar.grid(row=20, column=2, sticky="ns")
                self.save_peaks.grid(row=25,column=1)
                self.export_data_1st_detection.grid(row=30, column=1)
                self.show_saved_peaks.grid(row=28, column=1)
                self.delete_this_peak_button.grid(row=29, column=1)
      
        self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('peak detection: ', round(end-start, 2), ' seconds')
# =============================================================================
#       3rd tab buttons and functions  
# =============================================================================
    def filePDB(self):
        """loads pdb file and prints the path out to the user """
        
        self.pdb=askopenfilename(initialdir =  "", title = "Select A File",
                                              filetypes =(("pdb file", "*.pdb"),("pdb", "*.pdb"),("pdb","*.pdb*")) ) 
        path_pdb=self.pdb.split('/')
        label_path_pdb= Label(self.subframe, text=str(path_pdb[-1]),  width=40,  bg='lightblue')
        label_path_pdb.grid(row=4, column=1)
        
        start_, end_, self.structure= e.e2e_PDB(self.pdb)
        self.entry_coords.delete(0,END)
        self.entry_coords.insert(0, start_ + '-' + end_)
        self.dcd_button.grid(row=5,column=1)  
        
        self.entry_residues_end.delete(0, END)
        self.entry_residues_start.delete(0, END)
        self.entry_residues_start.insert(0, start_)
        self.entry_residues_end.insert(0, end_)
        
        self.entry_residues_end_work.delete(0, END)
        self.entry_residues_start_work.delete(0, END)
        self.entry_residues_start_work.insert(0, start_)
        self.entry_residues_end_work.insert(0, end_)


    def fileDCD(self):
        "loads dcd file and print the path to the user "
        path_dcd=askopenfilenames(initialdir =  "", title = "Select A File",
                                              filetypes =[("dcd file", "*.dcd"),("dcd", "*.dcd"),("dcd","*.dcd*") ])

        self.dcd= path_dcd[0]
        with open('all_dcds.dcd', 'w+b') as all_dcd: 
            for dcd in path_dcd:
                dcds= dcd.split("/")[-1]
                dcd_label="".join(dcds)
                with open(dcd, 'rb') as infile:
                    all_dcd.write(infile.read()) 

        for dcd in path_dcd:
            dcds= dcd.split("/")[-1]
            dcd_label="".join(dcds)


        self.compute_extensions_button.grid(row=10, column= 1)
        self.pb2 = ttk.Progressbar(self.subframe, orient=HORIZONTAL, length=300,  mode='determinate')
        self.pb2.grid(row=6, columnspan=3, pady=20)
        for i in range(2):
            self.subframe.update_idletasks()
            self.pb2['value'] += 60
            time.sleep(1)
        
        label_path_dcd= Label(self.subframe, text=str(dcd_label), width=40,  bg='lightblue')
        label_path_dcd.grid(row=6, column=1)
        #compute the extension+
        self.compute_extensions()
        #plot extension vs force 
        self.plot_extension_force()
        #plot derivative of extension
        self.plot_derivative_from_extension()
        self.pb2.destroy()

    def create_buttons_tab3(self):

        
        label_coordinates=Label(self.subframe_tab3, text= 'Enter residue IDs '+ "\n" +'e.g., coordinates of 2 domains:'+ "\n" + '1-83,  84-185', font=("Courrier", 10),width=50,  fg="blue")
        label_coordinates.grid(row=6, column=1)
        #initiliaze coords
        self.coords=StringVar()
        self.entry_coords=Entry(self.subframe_tab3, textvariable=self.coords, width=30 , font=("Courrier", 10))
        self.entry_coords.grid(row=7, column=1)
        
        self.compute_extensions_button=Button(self.subframe_tab3, text='3- Compute the extensions',font=("Courrier", 10)
                                         ,width=30, command=self.compute_extensions)
        
        self.label_help_extensions = Label(self.subframe_tab3, text="?", font=("Courrier", 10), bg='lightblue', cursor="hand2") 
        self.label_help_extensions.grid(row=7,column=2)
        self.label_help_extensions.bind("<Enter>", self.on_enter_extensions)
        self.label_help_extensions.bind("<Leave>", self.on_leave_extensions)
        
        self.export_extensions=Button(self.subframe_tab3, text='export data ',
                 font=("Courrier", 10),width=30, command=self.export_extensions_force_time)
        
        self.button_plot_force_extension=Button(self.subframe_tab3, text='plot Force vs extension ',
                 font=("Courrier", 10),width=30, command=self.plot_extension_force)
        self.button_plot_extension_time=Button(self.subframe_tab3, text='plot extension vs time [s] ',
                 font=("Courrier", 10),width=30, command=self.plot_extension_vs_time)

    def on_enter_extensions(self, event):
        self.label_help_extensions.configure(text="If you don't put any coordinates"+'\n'+" end to end extension will be computed ", bg='white', font='Courrier')

    def on_leave_extensions(self, enter):
        self.label_help_extensions.configure(text="?", bg='lightblue')
# =============================================================================
#         Computing the extension
# =============================================================================
    def compute_extensions(self):
        """
        This function gets the coordinates and computes
        the extension
        """
        start=time.time()
        try:
            self.dcd
            self.pdb
        except:
            tkinter.messagebox.showinfo("Warning ", "Please load PDB and DCD files")
        else:
            
            self.extensions={'extensions': []}
        
        raw_coords=self.coords.get()
        coord=raw_coords.split('-')
        coord=','.join(coord)
        coord=list(coord.split(','))
            
        try:
            self.df['-F[pN]']
            if self.force_tcl_tab2.get()==1:
                if not raw_coords:
                    start_, end_, self.structure= e.e2e_PDB(self.pdb)
                    self.entry_coords.insert(0, start_ + '-' + end_)
                    tkinter.messagebox.showinfo("information ", "extension from end to end will be computed"+'\n'+"Please, press 'Compute extensions' button again")
                    self.export_extensions.grid(row=45 , column=1)
                else:
                    self.e2e, self.timeInS, self.force, self.coords_atoms =e.extension_domain(self.dcd, self.structure, self.df['timeInS'],self.df['-F[pN]'], str(coord[0]), str(coord[1]) )
                    for i in range(0, len(coord)-1, 2):
                        self.extensions['extensions'].append(e.extension_domain(self.dcd, self.structure, self.df['timeInS'], self.df['-F[pN]'], str(coord[i])
                                                                        , str(coord[i+1]))[0])
            else:
                if not raw_coords:
                    start_, end_, self.structure= e.e2e_PDB(self.pdb)
                    self.entry_coords.insert(0, start_ + '-' + end_)
                    tkinter.messagebox.showinfo("information ", "extension from end to end will be computed"+'\n'+"Please, press 'Compute extensions' button again")
                    self.export_extensions.grid(row=45 , column=1)
                else:         
                    self.e2e, self.timeInS, self.force, self.coords_atoms =e.extension_domain(self.dcd, self.structure, self.df['timeInS'],self.df['F[pN]'], str(coord[0]), str(coord[1]) )
                    for i in range(0, len(coord)-1, 2):
                        self.extensions['extensions'].append(e.extension_domain(self.dcd, self.structure, self.df['timeInS'], self.df['F[pN]'], str(coord[i])
                                                                            , str(coord[i+1]))[0])
                
        except:

            if not raw_coords:
                start_, end_, self.structure= e.e2e_PDB(self.pdb)
                self.entry_coords.insert(0, start_ + '-' + end_)
                tkinter.messagebox.showinfo("information ", "extension from end to end will be computed"+'\n'+"Please, press 'Compute extensions' button again")
                self.export_extensions.grid(row=45 , column=1)
            else:
                self.e2e, self.timeInS, self.force, self.coords_atoms =e.extension_domain(self.dcd, self.structure, self.df['timeInS'],self.df['F[pN]'], str(coord[0]), str(coord[1]) )
                for i in range(0, len(coord)-1, 2):
                    self.extensions['extensions'].append(e.extension_domain(self.dcd, self.structure, self.df['timeInS'], self.df['F[pN]'], str(coord[i])
                                                                        , str(coord[i+1]))[0])
                
        self.export_extensions.grid(row=45 , column=1)
        
        self.entry_frames_start.delete(0, END)
        self.entry_frames_end.delete(0, END)
        
        self.entry_frames_start.insert(0, 0)
        self.entry_frames_end.insert(0, len(self.extensions['extensions'][0]))

        self.entry_frames_start_work.delete(0, END)
        self.entry_frames_end_work.delete(0, END)
        
        self.entry_frames_start_work.insert(0, 0)
        self.entry_frames_end_work.insert(0, len(self.extensions['extensions'][0]))
        
        self.button_plot_force_extension.grid(row=15 , column=1)
        self.button_plot_extension_time.grid(row=20 , column=1)
        self.plot_extension_force()

        self.plot_derivative_from_extension()
        tkinter.messagebox.showinfo("Information ", "The molecular extensions have been computed successfully")
        end=time.time()
        print('extension computing: ', round(end-start, 2), ' seconds')
# =============================================================================
#   Export information  
# =============================================================================
    def export_extensions_force_time(self):
        """
        exports the extension, force and time data in a csv file

        Returns
        -------
        None.

        """
        d={'extensions[nm]':[]}
        dico={'F[pN]':[], 'timeInS':[], '-F[pN]': []}
        raw_coords= self.coords.get()
        coord=raw_coords.split('-')
        coord=','.join(coord)
        coord=list(coord.split(','))
        length=len(self.extensions['extensions'][0])
        for i in range(len(self.extensions['extensions'])):
            d['extensions[nm]']=self.extensions['extensions'][i]
        dico['timeInS']=self.timeInS[0:length]
        dico['F[pN]']=self.df['F[pN]'][0:length]
        df=pd.DataFrame.from_dict(d)
        df.loc[:,'timeInS'] = pd.Series(dico['timeInS'])
        df.loc[:,'F[pN]'] = pd.Series(dico['F[pN]'])
        try:
            self.df['-F[pN]']    
            dico['-F[pN]']= self.df['-F[pN]'][0:length]
            df.loc[:,'-F[pN]'] = pd.Series(dico['-F[pN]'])

        except:
            print("saving F[pN], timeInSeconds and molecular extensions[nm]")
        for i in range(len(self.extensions['extensions'])):
            export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
            df.to_csv(export_file_path, sep='\t', index = False, header=True)
        print("Saved succussfly at: ", export_file_path)
# =============================================================================
#      extension   plots
# =============================================================================
    def plot_extension_force(self):
        """
        This function plot the extension vs force

        Returns
        -------
        None.

        """
        start=time.time()

        try:
            self.extensions
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, start by computing the extensions")

        else:
            if self.widget:
                self.widget.destroy()
            if self.toolbar:
                self.toolbar.destroy()
            figure = Figure(figsize=(7,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master= self.plot_frame_tab3)
            canvas.draw()
            self.widget= canvas.get_tk_widget()
            self.widget.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_tab3)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar = NavigationToolbar2Tk(canvas,   toolbarFrame) 
            self.toolbar.update()
            ax.set_xlabel('extension [nm]')
            ax.set_ylabel('force [pN]')
            if self.radiobutton_g_tab2.get()==1:
                ax.grid()

            raw_coords= self.coords.get()
            coord= raw_coords.split(',')
            if raw_coords:
                li= len(self.force)
                l=len(self.extensions['extensions'][0])    
                if l ==li :
                    colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                    for i in range(len(self.extensions['extensions'])):
                        ax.scatter( self.extensions['extensions'][i], self.force, s=10, color= colors['colors'][i], label= 'Coords  '+str(coord[i]))
                   
                    ax.legend()       
                else:
                    colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                    for i in range(len(self.extensions['extensions'])):
                        l=len(self.extensions['extensions'][i])
                        ax.scatter( self.extensions['extensions'][i], self.force[0: l], s=10, color= colors['colors'][i], label= 'Coords  '+str(coord[i]))
                    ax.legend()
            self.radiobutton_g_tab2.set(0)
        end= time.time()
        print('plot extension vs force ', round(end-start, 2) , ' seconds')
    
    def plot_extension_vs_time(self):
        """
        This function plot the extension vs time
        """        
        start=time.time()
        try:
            self.extensions
        except:
            tkinter.messagebox.showinfo("Warning ", "Please start by computing the extensions ")
        else:
            if self.widget:
                self.widget.destroy()
            if self.toolbar:
                self.toolbar.destroy()
                
            figure = Figure(figsize=(7,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_tab3)
            canvas.draw()
            self.widget= canvas.get_tk_widget()
            self.widget.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_tab3)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar = NavigationToolbar2Tk(canvas, toolbarFrame) 
            self.toolbar.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('extension [nm]')
            raw_coords=self.coords.get()
            if self.radiobutton_g_tab2.get()==1:
                ax.grid()
            if raw_coords:
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                coord= raw_coords.split(',')
                l=len(self.extensions['extensions'][0])
                for i in range(len(self.extensions['extensions'])):
                    ax.plot(self.timeInS[0 : l], self.extensions['extensions'][i], color=colors['colors'][i], label='Coords'+str(coord[i]))
                ax.legend(loc= "upper left")
            self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('extension vs timeStep ', round(end-start, 2), ' seconds ')
        
# =============================================================================
#  tab:     peak detection from the extension ==> buttons and functions 
# =============================================================================
    def create_buttons_detection_from_extension(self):
        """
        Creates all the buttons of the 'peak detection extension' tab
        """
        self.height=DoubleVar()
        self.smoothing_extension=IntVar()
        self.number=IntVar()

        label_smoothing_window=Label(self.subframe_tab_2nd_detection, text='Give value for smoothing window', font=("Courrier", 10),width=35)  
        label_smoothing_window.grid(row=2, column=1)

        entry_smoothing_window=Entry(self.subframe_tab_2nd_detection, textvariable=self.smoothing_extension, width=35, font=("Courrier", 10))  
        entry_smoothing_window.grid(row=3, column=1)
        
        label_threshold=Label(self.subframe_tab_2nd_detection, text='Give height threshold', font=("Courrier", 10),width=35)  
        label_threshold.grid(row=4, column=1)
                
        entry_height=Entry(self.subframe_tab_2nd_detection, textvariable=self.height,width=35, font=("Courrier", 10))  
        entry_height.grid(row=5, column=1)
        
        entry_number_highest_peaks=Entry(self.subframe_tab_2nd_detection, textvariable=self.number,font=("Courrier", 10),width=35)
        entry_number_highest_peaks.grid(row=7, column=1)
        
        label_number_highest_peaks=Label(self.subframe_tab_2nd_detection, text= 'or  Give N number of peaks',font=("Courrier", 10),width=35)
        label_number_highest_peaks.grid(row=6, column=1)

        button_plot_derivative=Button(self.subframe_tab_2nd_detection, text='1- plot derivative of extension',  font=("Courrier", 10),width=35,
                 command=self.plot_derivative_from_extension)
        button_plot_derivative.grid(row=9, column=1)

        self.button_detect_peaks_from_extension=Button(self.subframe_tab_2nd_detection, text='2- Detect peaks', font=("Courrier", 10),width=35,
                 command=self.plot_peaks_derivative)

        reset=Button(self.subframe_tab_2nd_detection, text= 'Reset', font=("Courrier", 10),width=35, command=self.delete_extension_peaks_table)
        reset.grid(row=10, column=1)
        
        self.label_help_tab=Label(self.subframe_tab_2nd_detection, text='?',  font=("Courrier", 10),width=35, bg='lightblue')
        self.add_peak_button=Button(self.subframe_tab_2nd_detection, text='Save selected peaks', font=("Courrier", 10),width=35, command=self.add_peak_to_list)
        self.plot_selected_peaks=Button(self.subframe_tab_2nd_detection, text='3- Plot selected peaks'+'\n`'+' on force-time curve', font=("Courrier", 10),width=35,
                 command=self.plot_selected_peaks_on_force_time)

        self.button_checklist=Button(self.subframe_tab_2nd_detection, text='Check list of all peaks', width=35, font=("Courrier", 10), command=self.show_list_total_peaks)
        self.link_checklist = Label(self.subframe_tab_2nd_detection, text="?", font=("Courrier", 10), bg='lightblue', cursor="hand2") 
        self.link_checklist.bind("<Enter>", self.on_enter_checklist)
        self.link_checklist.bind("<Leave>", self.on_leave_checklist)

        self.delete_from_list=Button(self.subframe_tab_2nd_detection, text= 'Delete selected peak(s)', width=35, font=("Courrier", 10), command=self.delete_from_total_list)
        self.button_export_2nd_detection=Button(self.subframe_tab_2nd_detection, text='export data' , font=("Courrier", 10),width=35, command=self.export_slopes_second_detection)

    def delete_extension_peaks_table(self):
        """
        This function resets the treeview of peak detection
        from the extension

        """
        self.tab.delete(*self.tab.get_children())
        self.number.set(0)
        self.smoothing_extension.set(0)
        self.height.set(0)

    def on_enter_tab(self, event):
        """
        Information appear when the user hovers over the label '?'

        Parameters
        ----------
        event : string
            Enter event (when the user hovers over the label).

        """
        self.label_help_tab.configure(text="Click to select peaks"+'\n'+ 'Click on colums to sort ', bg='white', font='Helvetica 8 bold')

    def on_leave_tab(self, enter):
        """
        The label '?' appears again once the user moves the mouse

        Parameters
        ----------
        enter : TYPE
            DESCRIPTION.

        """
        self.label_help_tab.configure(text="?", bg='lightblue')
 
    def treeview_sort_column(self, tv, col, reverse):
        """
        This function sorts the values of each column of the 
        treeview

        Parameters
        ----------
        tv : treeview
            the treeview of peaks from 1st detection, 2nd detection
            and refolding detection .
        col : treeview column

        reverse : bool
            false if ascending sort.
        """
        l = [(tv.set(k, col), k) for k in tv.get_children('')]
        l.sort(reverse=reverse)
        for index, (val, k) in enumerate(l):
            tv.move(k, '', index)

# =============================================================================
#         plot the derivative of extension
# =============================================================================
    def plot_derivative_from_extension(self):
        """
        plots the derivative of the extension
        - The function that calculates the derivative is called from extension.py file
        """
        start=time.time()
        if not self.coords.get():
            tkinter.messagebox.showinfo("Warning ", "Please set coordinates ")
        try:
            self.extensions
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, go to 'extensions' tab and press the button 'compute the extensions' ")
        else:
            if self.widget_2nd_detection:
                self.widget_2nd_detection.destroy()

            if self.toolbar_2nd_detection:
                self.toolbar_2nd_detection.destroy()

            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_2nd_detection)
            canvas.draw()
            self.widget_2nd_detection= canvas.get_tk_widget()
            self.widget_2nd_detection.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_2nd_detection)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_2nd_detection = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
            self.toolbar_2nd_detection.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('height')

            if self.radiobutton_g_tab2.get()==1:
                ax.grid()
            if not self.smoothing_extension.get():

                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                self.derivative={'derivative':[]}
                self.x, derivative, ts, self.factor=e.compute_derivative(self.extensions['extensions'][0], self.timeInS)
                length_extensions_list=len(self.extensions['extensions'])
                
                coords1=self.coords.get()
                coord= coords1.split(',')
                
                for i in range(length_extensions_list):

                    self.derivative['derivative'].append(e.compute_derivative(self.extensions['extensions'][i], self.timeInS)[1])

                l=len(self.derivative['derivative'][0])
                li=len(self.x)

                for i in range(len(self.derivative['derivative'])):
                    ax.plot(self.x, self.derivative['derivative'][i][0:li], color=colors['colors'][i], label='Coords '+coord[i])
                ax.legend()

                
            else:
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                window=self.smoothing_extension.get()
                self.derivative={'derivative':[]}
                d={ 'smooth': []}
                self.x, derivative, ts, self.factor=e.compute_derivative(self.extensions['extensions'][0], self.timeInS)
                length_extensions_list=len(self.extensions['extensions'])
                
                coords1=self.coords.get()
                coord= coords1.split(',')
                for i in range(length_extensions_list):

                    self.derivative['derivative'].append(e.compute_derivative(self.extensions['extensions'][i], self.timeInS)[1])
                for i in self.derivative['derivative']:
                    d['smooth'].append(gaussian_filter(i, window))

                l=len(self.derivative['derivative'][0])
                li=len(self.x)
                
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i], label='Coords '+str(coord[i]))
               #     print(coord[i])
            #pack plot peak of the derivative button
            self.button_detect_peaks_from_extension.grid(row=12 , column=1)
            self.plot_selected_peaks.grid(row=13 , column=1)
            ax.legend()
            self.radiobutton_g_tab2.set(0)
            end=time.time()
            print('plot derivative of extension ', round(end-start, 2), ' seconds')
# =============================================================================
#     plot selected peaks 
# =============================================================================
    def plot_selected_peaks_on_force_time(self):
        """
        gets selected peaks from the treeview and plots 
        them on force-time curve

        """
        try:
            self.peaks
        except:
            self.peaks=[]
        else:
            if self.widget_2nd_detection:
                self.widget_2nd_detection.destroy()
                
            if self.toolbar_2nd_detection:
                self.toolbar_2nd_detection.destroy()
                
            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_2nd_detection)
            canvas.draw()
            self.widget_2nd_detection= canvas.get_tk_widget()
            self.widget_2nd_detection.grid(row=10, column=1)
            
            # navigation toolbar
            toolbarFrame = Frame(master=  self.plot_frame_2nd_detection)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_2nd_detection= NavigationToolbar2Tk(canvas,  toolbarFrame) 
            self.toolbar_2nd_detection.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('force [pN]')
            if self.radiobutton_g_tab2.get() == 1:
                ax.grid()
            colors={1: 'darkblue' , 2: 'chocolate' , 3: 'green' , 4 :'red' , 5:'purple', 6:'gold', 7: 'gray', 8:'salmon' , 9:'yellowgreen' , 10:'brown'}
            raw_coords=self.coords.get()
            coord= raw_coords.split(',')

            ts= list(self.df['timeInS'])
            for i in range(len(ts)):
                ts[i]= "{:.2e}".format(ts[i])
                ts[i]=float(ts[i])

            curItem= self.tab.selection()
            curItems = [(self.tab.item(i)['values']) for i in curItem]
            try:
                if self.force_tcl_tab2.get() ==1:
                    self.df['-F[pN]']
                    f=self.df['-F[pN]']
                    d={'real_peak':[], 'selected_peaks': [], 'timeInS': [], 'F': [], 'Residue selection':[]} 
                    selection=[]
                    sel=[]
                    for i in curItems:
                        d['Residue selection'].append(i[2])
                        selection.append(float(i[0]))
        
                    for i in range(len(selection)):
                        sel.append(np.array(ts).flat[np.abs( np.array(ts) - selection[i]).argmin()])
                        d['selected_peaks'].append(list(ts).index(sel[i]))
        
                    ax.plot(ts, f)
        
                    for i in range(len(d['selected_peaks'])):
                        ax.plot(ts[int(d['selected_peaks'][i])], f[int(d['selected_peaks'][i])], 'o',  mew=5, ms=6, label='Coords '+str(coord[int(d['Residue selection'][i]-1)]), color=colors[d['Residue selection'][i]])
                        
                    handles, labels = figure.gca().get_legend_handles_labels()
                    by_label = dict(zip(labels, handles))
                    ax.legend(by_label.values(), by_label.keys())
                else:
                    f=self.df['F[pN]']
                    d={'real_peak':[], 'selected_peaks': [], 'timeInS': [], 'F': [], 'Residue selection':[]} 
                    selection=[]
                    sel=[]
                    for i in curItems:
                        d['Residue selection'].append(i[2])
                        selection.append(float(i[0]))
        
                    for i in range(len(selection)):
                        sel.append(np.array(ts).flat[np.abs( np.array(ts) - selection[i]).argmin()])
                        d['selected_peaks'].append(list(ts).index(sel[i]))  #gets index of peak
                        
        
                    ax.plot(ts, f)
        
                    for i in range(len(d['selected_peaks'])):
                        ax.plot(ts[int(d['selected_peaks'][i])], f[int(d['selected_peaks'][i])], 'o',  mew=5, ms=6, label='Coords '+str(coord[int(d['Residue selection'][i]-1)]), color=colors[d['Residue selection'][i]])
                       # print(self.extensions['extensions'][d['selected_peaks'][i]])
                    handles, labels = figure.gca().get_legend_handles_labels()
                    by_label = dict(zip(labels, handles))
                    ax.legend(by_label.values(), by_label.keys())
                    
            except:
                f=self.df['F[pN]']
                d={'real_peak':[], 'selected_peaks': [], 'timeInS': [], 'F': [], 'Residue selection':[]} 
                selection=[]
                sel=[]
                for i in curItems:
                    d['Residue selection'].append(i[2])
                    selection.append(float(i[0]))
    
                for i in range(len(selection)):
                    sel.append(np.array(ts).flat[np.abs( np.array(ts) - selection[i]).argmin()])
                    d['selected_peaks'].append(list(ts).index(sel[i]))
                    
    
                ax.plot(ts, f)
    
                for i in range(len(d['selected_peaks'])):
                    ax.plot(ts[int(d['selected_peaks'][i])], f[int(d['selected_peaks'][i])], 'o',  mew=5, ms=6, label='Coords '+str(coord[int(d['Residue selection'][i]-1)]), color=colors[d['Residue selection'][i]])
                    
                handles, labels = figure.gca().get_legend_handles_labels()
                by_label = dict(zip(labels, handles))
                ax.legend(by_label.values(), by_label.keys())

            self.peaks=list(self.peaks)
            self.radiobutton_g_tab2.set(0)
# =============================================================================
#   plots the peaks of the derivative    
# =============================================================================
    def plot_peaks_derivative(self):
        """
        plots the detected peaks on the derivative of extension
        """
        start=time.time()
        if not self.coords.get() :
            tkinter.messagebox.showinfo("Warning ", "Please set coordinates of selections ")
        try:
            self.derivative
        except:

            tkinter.messagebox.showinfo("Warning ", "Please follow the steps in the right order, Start by plotting the derivative")
        else:
            if self.widget_2nd_detection:
                self.widget_2nd_detection.destroy()
            if self.toolbar_2nd_detection:
                self.toolbar_2nd_detection.destroy()
            
            li=len(self.x)
            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_2nd_detection)
            canvas.draw()
            self.widget_2nd_detection= canvas.get_tk_widget()
            self.widget_2nd_detection.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master=  self.plot_frame_2nd_detection)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_2nd_detection = NavigationToolbar2Tk(canvas, toolbarFrame) 
            self.toolbar_2nd_detection.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('Height')
            li= len(self.x)
            if self.radiobutton_g_tab2== 1:
                ax.grid()
            colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
            n_highest_number= self.number.get()
            
            if not n_highest_number :
                height=self.height.get()
                window=self.smoothing_extension.get()
                d={ 'derivative':[], 'smooth': [], 'y':[], 'idx' : []}
                for i in self.derivative['derivative']:    
                    d['smooth'].append(gaussian_filter(i, window))
                peaks={'peaks':[] , 'domain': []  }
                properties=[]
                l=len(self.derivative['derivative'][0])
                start_peak=[]
                raw_coords=self.coords.get()
                coord= raw_coords.split(',')
            #find peaks
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i], label='Coords '+str(coord[i]), zorder=0)
                    peaks['peaks'].append( signal.find_peaks(d['smooth'][i], height=height)[0])
                    peaks['domain'].append(i+1)


                list_all_heights=[]
                for i in range(len(peaks['peaks'])):
                    for j in range(len(peaks['peaks'][i])):
                        list_all_heights.append(d['smooth'][i][peaks['peaks'][i][j]])

                halves=[]
                for height in list_all_heights:
                    halves.append(height/2)
                self.tab.delete(*self.tab.get_children())
                self.tab.heading('#2', text='time [s]')
                self.tab.heading('#3', text='derivative at peak') 
                self.x = np.array(self.x) 
                
                no_root=0
                peaks['peaks'] = [x for l in peaks['peaks'] for x in l]
                peaks['peaks']= sorted(peaks['peaks'])
                peaks['peaks']= np.append(0, peaks['peaks'])
                for i in range(len(peaks['peaks'])):
                    for j in range(len(d['smooth'])):
                        half = d['smooth'][j][peaks['peaks'][i]] / 2 
                        d['smooth'][j]= np.array(d['smooth'][j])

                        roots=func.find_roots(self.x[peaks['peaks'][i-1]:peaks['peaks'][i]], d['smooth'][j][peaks['peaks'][i-1]:peaks['peaks'][i]] - half)
                        if len(roots) > 0:
                            ax.scatter(roots[-1], half,  s=20, color='red')
                            self.tab.insert('',i ,values=("{:.2e}".format(roots[-1]), "{:.2e}".format(half), peaks['domain'][j]))
                        else:
                                no_root= no_root +1
                if n_highest_number > no_root:
                    tkinter.messagebox.showinfo('information' ,' Only '+ str(number-no_root+1) + ' peaks were detected')

                for col in self.colnames:
                    self.tab.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab, _col, False))
                self.tab.grid(row=20, column=1, sticky=W+E+S+N)
                self.s.grid(row=20, column=1, sticky=E)
                self.label_help_tab.grid(row=20, column=1, sticky= E)
                self.label_help_tab.bind("<Enter>", self.on_enter_tab)
                self.label_help_tab.bind("<Leave>", self.on_leave_tab)
                ax.legend()
                
            elif n_highest_number:
                height=self.height.get()
                window=self.smoothing_extension.get()
                d={ 'derivative':[], 'smooth': [], 'y':[], 'idx' : []}
                
                for i in self.derivative['derivative']:
                    d['smooth'].append(gaussian_filter(i, window))
                peaks={'peaks':[] , 'domain': []  }
                properties=[]
                l=len(self.derivative['derivative'][0])
                start_peak=[]
                raw_coords=self.coords.get()
                coord= raw_coords.split(',')
            #find peaks
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i], label='Coords '+str(coord[i]), zorder=0)
                    peaks['peaks'].append( signal.find_peaks(d['smooth'][i], height=height)[0])
                    peaks['domain'].append(i+1)
                list_all_heights=[]
                for i in range(len(peaks['peaks'])):
                    for j in range(len(peaks['peaks'][i])):
                        list_all_heights.append(d['smooth'][i][peaks['peaks'][i][j]])

                halves=[]
                list_roots={}
                for height in list_all_heights:
                    halves.append(height/2)
                self.tab.delete(*self.tab.get_children())
                self.tab.heading('#2', text='time [s]')
                self.tab.heading('#3', text='derivative at peak') 
                self.x = np.array(self.x) 
                no_root=0
                peaks['peaks'] = [x for l in peaks['peaks'] for x in l]
                peaks['peaks']= sorted(peaks['peaks'])
                peaks['peaks']= np.append(0, peaks['peaks'])
                for i in range(len(peaks['peaks'])):
                    for j in range(len(d['smooth'])):
                        half = d['smooth'][j][peaks['peaks'][i]] / 2 
                        d['smooth'][j]= np.array(d['smooth'][j])
                        roots=func.find_roots(self.x[peaks['peaks'][i-1]:peaks['peaks'][i]], d['smooth'][j][peaks['peaks'][i-1]:peaks['peaks'][i]] - half)
                        if len(roots) > 0:
                            list_roots[roots[-1]]=(half, peaks['domain'][j])
                            pairs= func.extract_N_largest_keys(list_roots, n_highest_number)
                           # print(pairs)
                
                for key, (value, domain) in pairs:
                    ax.scatter(key , value,  s=20, color='red')
                    self.tab.insert('',i ,values=("{:.2e}".format(key), "{:.2e}".format(value), domain))
                                    
                if n_highest_number > len(pairs):
                        
                    tkinter.messagebox.showinfo("informations ", "only "+str(len(pairs)) + " peaks have been detected")

                for col in self.colnames:
                    self.tab.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab, _col, False))
                self.tab.grid(row=20, column=1, sticky=W+E+S+N)
                self.s.grid(row=20, column=1, sticky=E)
                self.label_help_tab.grid(row=20, column=1, sticky= E)
                self.label_help_tab.bind("<Enter>", self.on_enter_tab)
                self.label_help_tab.bind("<Leave>", self.on_leave_tab)
        
                self.tab.grid(row=20, column=1, sticky=W+E+S+N)
                self.s.grid(row=20, column=1, sticky=E)
                self.label_help_tab.grid(row=20, column=1, sticky=E)
                self.label_help_tab.bind("<Enter>", self.on_enter_tab)
                self.label_help_tab.bind("<Leave>", self.on_leave_tab)
                ax.legend()
            self.radiobutton_g_tab2.set(0)
        self.add_peak_button.grid(row=25,column=1)
        self.button_checklist.grid(row=26, column=1)
        self.link_checklist.grid(row=26, column=1, sticky= E)
        self.delete_from_list.grid(row=27,column=1)
        end=time.time()
        print('peak detection ', round(end-start, 2), ' s')
# =============================================================================
# save peaks to list 2nd detection #save_peaks
# =============================================================================
    def add_peak_to_list(self):
        """
        puts the selected peak from the 2nd detection (extension approach)
        in a list to save them
        """
        try:
            self.peaks
        except:
            self.peaks=[]
        else:
            ts= list(self.df['timeInS'])
            for i in range(len(ts)):
                ts[i]= "{:.2e}".format(ts[i])
                ts[i]=float(ts[i])

            sel =[]
            curItem= self.tab.selection()
            curItems = [(self.tab.item(i)['values']) for i in curItem]
            user_selection=[]
            self.selection=[]
            for i in curItems:
                user_selection.append(float(i[0]))
                self.selection.append(i[-1])
            for i in range(len(user_selection)):
                sel.append(np.array(ts).flat[np.abs( np.array(ts) - user_selection[i]).argmin()]) 
                if list(ts).index(sel[i]) not in self.peaks:
                    self.peaks.append(list(ts).index(sel[i]))
                
            tkinter.messagebox.showinfo("informations ", "Added to list successfully")
        self.button_export_2nd_detection.grid(row=30, column=1)
    
    def on_enter_checklist(self, event):
        """
        Gives information about the use of the checklist when the user hovers

        Parameters
        ----------
        event : string
            <Enter>.
        """
        self.link_checklist.configure(text="When you save the peaks a pop up window will appear"+'\n'+"If the window does not appear click again on the 'save selected peaks' button"+'\n'+"To modify/delete some peaks click on 'Check the list ' button ", bg='white', font='Courrier')

    def on_leave_checklist(self, enter):
        """
        when the user leaves the label becomes ? (like in the initial state)

        Parameters
        ----------
        enter : string
            on leave.
        """
        self.link_checklist.configure(text="?", bg='lightblue')

    def export_slopes_second_detection(self):
        """
        This function puts the loading rates from the second peak detection 
        (the detection from the extension) in a csv file 
        """
        distance= self.distance.get()
        try:
            self.peaks
        except:
            tkinter.messagebox.showinfo('information' ,'Please, save peaks first')
        try:
            self.df['-F[pN]']
            if self.radiobutton_tcl_forces.get()==1:
                df_2nd_peak_detection , slopes_d2, intercepts_d2= func.compute_slopes_second_detection(self.df , self.df['-F[pN]'], self.velocity,distance, self.peaks, self.selection, self.e2e)
                export_file_path = filedialog.asksaveasfilename()
                df_2nd_peak_detection.to_csv( export_file_path, sep='\t',header=True, index=False, mode='a')
        except:
            print('saving ..........')
      #  else:
            df_2nd_peak_detection , slopes_d2, intercepts_d2= func.compute_slopes_second_detection(self.df , self.df['F[pN]'], self.velocity,distance, self.peaks, self.selection, self.e2e)
            export_file_path = filedialog.asksaveasfilename()
            df_2nd_peak_detection.to_csv( export_file_path, sep='\t',header=True, index=False, mode='a')

# =============================================================================
#     Check/delete list of peaks
# =============================================================================
    def list_total_peaks_1st_detection(self):
        """
        This function inserts all the SAVED peaks in the Treeview
        so that the user can check his list
        """
        self.ts= self.df['timeInS']
        self.f= self.df['F[pN]']
        self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
        default_distance_slope=1
        distance_slope= self.distance.get()
        try:
            self.df['-F[pN]']
            self.peaks
            if self.radiobutton_tcl_force.get() ==1:
                if not distance_slope:
                    d, slope, intercept = func.get_slopes(self.df['displacement[nm]'], self.df['timeInS'], self.df['-F[pN]'], self.peaks, default_distance_slope )
                else:
                    d, slope, intercept = func.get_slopes(self.df['displacement[nm]'], self.df['timeInS'], self.df['-F[pN]'], self.peaks,  distance_slope )

                for i in range(len(self.peaks)):
                    self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][self.peaks[i]]), "{:.2e}".format(self.df['-F[pN]'][self.peaks[i]]), "{:.2e}".format(slope[i]), "{:.2e}".format(slope[i]/ (self.velocity*10**9))))
                self.tab_peak_detection.grid(row=20, column=1, sticky=W+E+S+N)
                self.scroll_bar.grid(row=20, column=2, sticky="ns")           
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, Save peaks first ")
        else:
            if not distance_slope:
                d, slope, intercept = func.get_slopes(self.df['displacement[nm]'], self.df['timeInS'], self.df['F[pN]'], self.peaks, default_distance_slope )
            else:
                d, slope, intercept = func.get_slopes(self.df['displacement[nm]'], self.df['timeInS'], self.df['F[pN]'], self.peaks, distance_slope )

            for i in range(len(self.peaks)):
                self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][self.peaks[i]]), "{:.2e}".format(self.df['F[pN]'][self.peaks[i]]), "{:.2e}".format(slope[i]), "{:.2e}".format(slope[i]/ (self.velocity*10**9))))

            self.tab_peak_detection.grid(row=20, column=1, sticky=W+E+S+N)
            self.scroll_bar.grid(row=20, column=2, sticky="ns")            
        
    def delete_from_total_list_1st_detection(self):
        """
        This function deletes the peak selected by the user
        from the total list of peaks
        """ 
        dico={}
        for p in self.peaks:
            dico[p]=  "{:.2e}".format(self.df['timeInS'][p])
        reverse_dict = {v: k for k, v in dico.items()}
        print(reverse_dict)
        to_delete=[]
        curItem= self.tab_peak_detection.selection()
        curItems = [(self.tab_peak_detection.item(i)['values']) for i in curItem]
        for items in curItems:
            if items[1] in dico.values():
                self.peaks.remove(reverse_dict.get(items[1]))
        for i in curItem:
            self.tab_peak_detection.delete(i)
        self.tab_peak_detection.grid(row=20, column=1, sticky=W+E+S+N)
        self.scroll_bar.grid(row=20, column=2, sticky="ns")            
            
        #second peak detection
    def show_list_total_peaks(self):
        """
        This function inserts all the SAVED peaks in the Treeview
        so that the user can check his list
        """
        try:
            self.peaks
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, Save peaks first ")
        else:
            self.ts= self.df['timeInS']
            self.f= self.df['F[pN]']
            self.tab.delete(*self.tab.get_children())
            self.tab.heading('#1', text='time [s]')
            self.tab.heading('#2', text='force [pN]')

            for i in range(len(self.peaks)):
                self.tab.insert('',i ,values=( "{:.2e}".format( self.ts[self.peaks[i]]), "{:.2e}".format(self.f[self.peaks[i]])))
            self.tab.grid(row=20, column=1, sticky=W+E+S+N)
            self.s.grid(row=20, column=2, sticky="ns")

         #2nd peak detection   
    def delete_from_total_list(self):
        """
        This function deletes the peak selected by the user
        from the total list of peaks (Delete_peaks_second_detection)
        """
        dico={}
        for p in self.peaks:
            dico[p]=  "{:.2e}".format(self.df['timeInS'][p])
        reverse_dict = {v: k for k, v in dico.items()}
        print(reverse_dict)

        to_delete=[]
        curItem= self.tab.selection()
        curItems = [(self.tab.item(i)['values']) for i in curItem]
        for items in curItems:
            if items[0] in dico.values():

                self.peaks.remove(reverse_dict.get(items[0]))
        for i in curItem:
            self.tab.delete(i)

        self.tab.grid(row=20, column=1, sticky=W+E+S+N)
        self.s.grid(row=20, column=2, sticky="ns")
        
        
    def save_peaks_refolding(self):
        sel=[]
        ts= self.df['timeInS']
        try:
            self.refolding_peaks
        except:
            self.refolding_peaks=[]
        else:
            curItem= self.tab_refolding.selection()
            curItems = [(self.tab_refolding.item(i)['values']) for i in curItem]
                
            user_selection=[]
            self.refolding_selection=[]
            for i in curItems:
                user_selection.append(float(i[0]))
                self.refolding_selection.append(i[-1])
            for i in range(len(user_selection)):
                sel.append(np.array(ts).flat[np.abs( np.array(ts) - user_selection[i]).argmin()])
                if list(ts).index(sel[i]) not in self.refolding_peaks:
                    self.refolding_peaks.append(list(ts).index(sel[i]))
            tkinter.messagebox.showinfo("informations ", "Added to list successfully")
            self.button_export_peaks_refolding.grid(row=30, column=0)
           
           
    def export_data_refolding(self):
        """
        export refolding peaks to a csv file

        Returns
        -------
        None.

        """
        distance= self.distance.get()
        try:
            self.peaks
        except:
            tkinter.messagebox.showinfo('information' ,'Please, save peaks first')
        try:
            self.df['-F[pN]']
            if self.radiobutton_tcl_forces.get()==1:
                df_refolding_peak_detection , slopes_d2, intercepts_d2= func.compute_slopes_second_detection(self.df , self.df['-F[pN]'], self.velocity,distance, self.refolding_peaks, self.refolding_selection, self.e2e)
                export_file_path = filedialog.asksaveasfilename()
                df_refolding_peak_detection.to_csv( export_file_path, sep='\t',header=True, index=False, mode='a')
        except:
            print('saving ..........')
      #  else:
            df_refolding_peak_detection , slopes_d2, intercepts_d2= func.compute_slopes_second_detection(self.df , self.df['F[pN]'], self.velocity,distance, self.refolding_peaks, self.refolding_selection, self.e2e)
            export_file_path = filedialog.asksaveasfilename()
            df_refolding_peak_detection.to_csv( export_file_path, sep='\t',header=True, index=False, mode='a')

# =============================================================================
#  tab Refolding peaks buttons and function                 
# =============================================================================
    def create_buttons_refolding(self):
        """
        Creates all the buttons of the 'refolding peaks' tab
        """
        self.smoothing_value= IntVar()
        self.height_value= DoubleVar()
        self.number_peaks_refolding=IntVar()
    
        label_refolding=Label(self.subframe_tab_refolding, text='Give smoothing window (Gaussian filter)', width=30)
        label_refolding.grid(row=0, column= 0)
        
        entry=Entry(self.subframe_tab_refolding, textvariable=self.smoothing_value, width=30, font=("Courrier", 10))
        entry.grid(row=1, column= 0)
        
        label_height=Label(self.subframe_tab_refolding, text='Give height threshold', width=30, font=("Courrier", 10))
        label_height.grid(row=2, column= 0)
        entry_height=Entry(self.subframe_tab_refolding, textvariable=self.height_value, width=30, font=("Courrier", 10))
        entry_height.grid(row=3, column= 0)
        
        label_n_selection=Label(self.subframe_tab_refolding, text='or Give N number of peaks', width=30, font=("Courrier", 10))
        label_n_selection.grid(row=4, column= 0)
        entry_n_selection=Entry(self.subframe_tab_refolding, textvariable=self.number_peaks_refolding, width=30, font=("Courrier", 10))
        entry_n_selection.grid(row=5, column= 0)
        
        b=Button(self.subframe_tab_refolding, text='1- Detect refolding',font=("Courrier", 10),width=30, command=self.refolding_detection )
        b.grid(row=6 , column= 0)
        
        button_plot_force_time=Button(self.subframe_tab_refolding, text='2- plot on force vs time curve',  width=30,
                 font=("Courrier", 10), command=self.plot_refoldng_peaks_F_vs_time)
        button_plot_force_time.grid(row=7, column=0)
        
        button_save_refolding_peaks=Button(self.subframe_tab_refolding, text='3- save refolding peaks',  width=30,
                 font=("Courrier", 10), command=self.save_peaks_refolding)
        button_save_refolding_peaks.grid(row=8, column=0)
        
        self.button_export_peaks_refolding=Button(self.subframe_tab_refolding, text='4- export refolding data',  width=30,
                  font=("Courrier", 10), command=self.export_data_refolding)
        
        
        

    def plot_refoldng_peaks_F_vs_time(self):
        """
        This function plots the selected refolding peaks
        on the force vs time curve 
        """
        start=time.time()
        # remove old widgets
        if self.widget_refolding:
            self.widget_refolding.destroy()

        if self.toolbar_refolding:
            self.toolbar_refolding.destroy()
        
        figure = Figure(figsize=(6,5), dpi=100)
        ax=figure.add_subplot(111)
        ax.set_xlabel('time [s]')
        ax.set_ylabel('force [pN]')
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_refolding)
        canvas.draw()
        self.widget_refolding= canvas.get_tk_widget()
        self.widget_refolding.grid(row=10, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_refolding)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_refolding = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        figure.tight_layout()
        if self.radiobutton_g_tab2.get()==1:
            ax.grid()
            
        coords1=self.coords.get()
        coord= coords1.split(',')
        
        ts= list(self.df['timeInS'])
        for i in range(len(ts)):
            ts[i]= "{:.2e}".format(ts[i])
            ts[i]=float(ts[i])

        f=self.df['F[pN]']
        d={'selected_peaks': [], 'F' : [], 'timeInS': [], 'Residue selection': []} #selection is domain in our case
        curItem= self.tab_refolding.selection()
        curItems = [(self.tab_refolding.item(i)['values']) for i in curItem]
        user_selection=[]
        for i in curItems:
            user_selection.append(float(i[0]))
            d['Residue selection'].append(i[2])
        for i in range(len(user_selection)):
            d['selected_peaks'].append(ts.index(user_selection[i]))

        ax.plot(ts, f)
        colors={1: 'darkblue' , 2: 'chocolate' , 3: 'green' , 4 :'red' , 5:'purple', 6:'gold', 7: 'gray', 8:'salmon' , 9:'yellowgreen' , 10:'brown'}
        for i in range(len(d['selected_peaks'])):
            ax.plot(ts[int(d['selected_peaks'][i])], f[int(d['selected_peaks'][i])], 'o',  mew=5, ms=6, label='Coords '+str(coord[d['Residue selection'][i]-1]), color=colors[d['Residue selection'][i]])
            
        handles, labels = figure.gca().get_legend_handles_labels()
        by_label = dict(zip(labels, handles))
        ax.legend(by_label.values(), by_label.keys())

        self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('refolding peaks plotting ', round(end-start, 2), ' seconds ')

    def refolding_detection(self):
        """
        This function detects the refolding peaks. It get the height or number of peaks
        set by the user in the entries and detects peaks according to these values
        """
        start=time.time()
        if not self.coords.get():
            tkinter.messagebox.showinfo("Warning ", "Please, set coordinates")
        try:
            self.derivative

        except:
            tkinter.messagebox.showinfo("Warning ", "Please follow the steps in the right order, Start by plotting the derivative ")
        else:
            if self.widget_refolding:
                self.widget_refolding.destroy()

            if self.toolbar_refolding:
                self.toolbar_refolding.destroy()
            
            figure = Figure(figsize=(6,5), dpi=100)
            ax=figure.add_subplot(111)
            ax.set_xlabel('time [s]')
            ax.set_ylabel('Height')
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_refolding)
            canvas.draw()
            self.widget_refolding= canvas.get_tk_widget()
            self.widget_refolding.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_refolding)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_refolding = NavigationToolbar2Tk(canvas,   toolbarFrame) 
            window=self.smoothing_value.get()
            height=self.height_value.get()
            raw_coords=self.coords.get()
            number_highest_peaks= self.number_peaks_refolding.get()
            colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
            if self.radiobutton_g_tab2.get() == 1:
                ax.grid()
            if not number_highest_peaks:
                coord= raw_coords.split(',')
                d={ 'derivative':[], 'smooth': []}
            
                for i in self.derivative['derivative']:
                    d['smooth'].append(gaussian_filter(i, window))
                peaks={'peaks':[] , 'domain': [] }
                l=len(self.derivative['derivative'][0])
                li=len(self.x)
                
                self.tab_refolding.delete(*self.tab_refolding.get_children())
                #find peaks
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i], label='Coords '+str(coord[i]))
                    peaks['peaks'].append(signal.find_peaks(-d['smooth'][i], height=height)[0])
                    peaks['domain'].append(i+1)

                for i in range(len(peaks['peaks'])):
                    for j in range(len(peaks['peaks'][i])):
                        ax.plot(self.x[peaks['peaks'][i][j]], d['smooth'][i][peaks['peaks'][i][j]], 'or')
                        self.tab_refolding.insert('',i ,values=("{:.2e}".format(self.x[peaks['peaks'][i][j]]), "{:.2e}".format(d['smooth'][i][peaks['peaks'][i][j]]), peaks['domain'][i]))
                for col in self.colnames:
                    self.tab_refolding.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab_refolding, _col, False))
                self.tab_refolding.grid(row=20, column=0)   
                self.scroll.grid(row=20, column=0)
                ax.legend()
            elif number_highest_peaks:
                coord= raw_coords.split(',')
                d={ 'derivative':[], 'smooth': []}
                for i in self.derivative['derivative']:
                    
                    d['smooth'].append(gaussian_filter(i, window))
                peaks={'peaks':[] , 'domain': [] } 
                
                l=len(self.derivative['derivative'][0])
                li=len(self.x)
               
                properties=[]
            
            #find peaks
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i], label='Coords '+str(coord[i]))
                    peaks['domain'].append(i+1)
                    properties.append(signal.find_peaks(-d['smooth'][i], height=0)[1])  #height is equal to 0 because we want all heights of all peaks (no height criteria)
                    
                for i in range(len(d['smooth'])):
                   
                    peaks['peaks'].append(signal.find_peaks(-d['smooth'][i], height=0)[0])
                
                list_all_heights=[]
                for i in range(len(properties)):
                    for j in range(len(properties[i]['peak_heights'])):
                        list_all_heights.append(properties[i]['peak_heights'][j])
                          
                peak={'peak':[], 'domain': [], 'derivative':[]}
                list_all_heights=sorted(list_all_heights)

                list_all_heights= list_all_heights[-number_highest_peaks:]
                
                theset = frozenset(list_all_heights)
                theset = sorted(theset, reverse=True)
                thedict = {}
             
                #to avoid IndexError : make sure that the total number of heights is not less than the number that the user set
                if len(list_all_heights) < number_highest_peaks:
                    number_highest_peaks= len(list_all_heights)
                    tkinter.messagebox.showinfo('information' ,' Only '+ str(number_highest_peaks) + ' peaks were detected')
   
                for j in range(number_highest_peaks):
                    positions = [i for i, x in enumerate(list_all_heights) if x == theset[j]]
                    thedict[theset[j]] = positions
                    for i in range(len(properties)):
                        try: 
                            list(properties[i]['peak_heights']).index(theset[j])
                          #  print('1st = ' + str(theset[j]) + ' at ' + str(peaks['peaks'][i][list(properties[i]['peak_heights']).index(theset[j])]))
                            peak['peak'].append(peaks['peaks'][i][list(properties[i]['peak_heights']).index(theset[j])])
                            peak['domain'].append(i+1)
                        except:
                            l=[]
                #plot peaks and insert them in the table
                self.tab_refolding.delete(*self.tab_refolding.get_children())
                
                for i in range(len(peak['peak'])):
                    self.tab_refolding.insert('',i ,values=( "{:.2e}".format(self.x[peak['peak'][i]]), "{:.2e}".format(d['smooth'][peak['domain'][i]-1][peak['peak'][i]]), peak['domain'][i]))
                    ax.plot(self.x[peak['peak'][i]], d['smooth'][peak['domain'][i]-1][peak['peak'][i]], 'or')
                
                for col in self.colnames:
                    self.tab_refolding.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab_refolding, _col, False))
                self.tab_refolding.grid(row=20, column=0, sticky=W+E+S+N)
                self.scroll.grid(row=20, column=0, sticky=E)

                ax.legend()
            self.radiobutton_g_tab2.set(0)
            end=time.time()
            print('refolding peaks detection ', round(end-start, 2), ' seconds ')
    
# =============================================================================
# tab: slopes buttons and functions 
# =============================================================================
    def create_buttons_slopes(self):
        """
        This function creates all the buttons of the slopes tab
        """
        self.distance=DoubleVar()

        label=Label(self.subframe_tab2, text= 'Set distance in nm', font=("Courrier", 10),  width=30)
        label.grid(row=4, column=2)
        
        ent= Entry(self.subframe_tab2, textvariable=self.distance, width=10, font=("Courrier", 10))
        ent.delete(0,END)
        ent.insert(0, 1)
        ent.grid(row=5,column=2)
        b=Button(self.subframe_tab2, text='5- plot LoadingRate(pN/s)' + '\n'+'(slopes)'  + '\n'+'at distance above',
                 font=("arial", 9),width=18, command=self.plot_slopes_at_dist)
        b.grid(row=6 , column=2)

    def plot_slopes_at_dist(self):
        """
        plots the slopes at the distance that the user gives 
        """
        start=time.time()
        distance=self.distance.get()
        prominence=self.prominence.get()
        try:
            self.peaks
        except:
            tkinter.messagebox.showinfo("Warning ", "Please,  save peaks before plotting the slopes ")
        else:
            if len(self.peaks)==0:
                tkinter.messagebox.showinfo("Warning ", "Your list is empty ! Save the peaks of your interest first")
    
            if self.widget_tab2:
                self.widget_tab2.destroy()

            if self.toolbar_tab2:
                self.toolbar_tab2.destroy()
            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_tab2)
            canvas.draw()
            self.widget_tab2= canvas.get_tk_widget()
            self.widget_tab2.grid(row=10, column=1)

            toolbarFrame = Frame(master= self.plot_frame_tab2)            
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_slopes = NavigationToolbar2Tk(canvas,  toolbarFrame) 
            self.toolbar_slopes.update()
            ax.set_ylabel('Force [pN]')
            ax.set_xlabel('time [s]')
            if self.radiobutton_g_tab2.get()==1:
                ax.grid()

            self.peaks=sorted(self.peaks)
            ts= self.df['timeInS']
            
            if self.force_tcl_tab2.get()==1:
                d, slope,intercept= func.get_slopes( self.df['displacement[nm]'] , self.df['timeInS'], self.df['-F[pN]'], self.peaks, distance)
                ax.plot(self.df['timeInS'], self.df['-F[pN]'])
                self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                for i in range(len(self.peaks)):
                    self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][self.peaks[i]]), "{:.2e}".format(self.df['-F[pN]'][self.peaks[i]]), "{:.2e}".format(slope[i] ), "{:.2e}".format(slope[i]/ (self.velocity*10**9))))


            else:
                d, slope,intercept=func.get_slopes(self.df['displacement[nm]'] ,  self.df['timeInS'], self.df['F[pN]'], self.peaks, distance)
                ax.plot(self.df['timeInS'], self.df['F[pN]'])
                self.tab_peak_detection.delete(*self.tab_peak_detection.get_children()) 
                for i in range(len(self.peaks)):
                    self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][self.peaks[i]]), "{:.2e}".format(self.df['F[pN]'][self.peaks[i]]), "{:.2e}".format(slope[i] ), "{:.2e}".format(slope[i]/ (self.velocity*10**9))))


            for i in range(len(d['peaks'])):
                ax.plot(ts[d['index_dis2'][i]:d['peaks'][i]], slope[i]*ts[d['index_dis2'][i]:d['peaks'][i]]+intercept[i],'r')
            
            # Flag to keep track if the condition is satisfied (peak overlap here)
            peaks_overlap = False
            for i in range(1, len(d['peaks'])):
                if d['index_dis2'][i] < d['peaks'][i-1]:
                    peaks_overlap = True
                    break
            if peaks_overlap:
                tkinter.messagebox.showinfo("Warning", "You have two overlapping peaks, please set a small distance")
            self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('slopes ', round(end-start, 2), ' seconds')
    
    def export_slopes(self):
        """
        This function exports the slopes in the slopes tab
        """
        distance= self.distance.get()
        if self.radiobutton_g_tab2.get()==1:
            try:
                self.df['-F[pN]']
                df_data= func.slopes_at_peaks(self.df['displacement[nm]'], self.df['timeInS'], self.df['-F[pN]'], self.peaks, distance,self.df['timeStep']  , self.velocity*10**9)

            except:
                tkinter.messagebox.showinfo("Warning ", "If no TCL forces have been recorded untick 'F(-v)'")
        
        else:
            df_data= func.slopes_at_peaks(self.df['displacement[nm]'], self.df['timeInS'], self.df['F[pN]'], self.peaks, distance,self.df['timeStep']  , self.velocity*10**9)
        export_file_path = filedialog.asksaveasfilename()
        df_data.to_csv( export_file_path, sep='\t',header=True, index=False, mode='a')

    def create_buttons_correlation(self):
            """
            creates all buttons of the last tab: compute correlations between pairs of residues
            """

            calculate_button = Button(self.subframe_tab_correlation, text="1- Calculate Correlations", font=("Courrier", 10)
                              ,width=30, command= self.calculate_correlations)
            calculate_button.grid(row=20,column=2)
            
            self.cutoff= IntVar()
            entry_cutoff=Entry(self.subframe_tab_correlation, textvariable=self.cutoff, width=30, font=("Courrier", 10))
            entry_cutoff.delete(0, END)
            entry_cutoff.insert(0, 13)
            entry_cutoff.grid(row=2, column= 2)
            
            label_cutoff = Label(self.subframe_tab_correlation, text="Distance cutoff in angstroms", font=("Courrier", 10)    ,width=30)
            label_cutoff.grid(row=1,column=2)
            
            export_button = Button(self.subframe_tab_correlation, text="2- Export correlation values", font=("Courrier", 10)  ,width=30, command=self.export_correlations)
            export_button.grid(row=30,column=2)

            label_residues = Label(self.subframe_tab_correlation, text="RESIDUES:", font=("Courrier", 12) ,width=30)
            label_residues.grid(row=4,column=2)
              
            label_residues_start = Label(self.subframe_tab_correlation, text="FROM", font=("Courrier", 10) ,width=30)
            label_residues_start.grid(row=5,column=2)
 
            label_residues_end = Label(self.subframe_tab_correlation, text="TO", font=("Courrier", 10)     ,width=30)
            label_residues_end.grid(row=5,column=3)

            self.residues_start= IntVar()
            self.entry_residues_start=Entry(self.subframe_tab_correlation, textvariable=self.residues_start, width=30, font=("Courrier", 10))
            self.entry_residues_start.delete(0, END)
            self.entry_residues_start.grid(row=6, column= 2)

            self.residues_end= IntVar()
            self.entry_residues_end=Entry(self.subframe_tab_correlation, textvariable=self.residues_end, width=30, font=("Courrier", 10))
            self.entry_residues_end.delete(0, END)

            self.entry_residues_end.grid(row=6, column= 3)

            self.residues_start_second_range= IntVar()
            self.entry_residues_start_second_range=Entry(self.subframe_tab_correlation, textvariable=self.residues_start_second_range, width=30, font=("Courrier", 10))
            self.entry_residues_start_second_range.grid(row=7, column= 2)
    
            self.residues_end_second_range= IntVar()
            self.entry_residues_end_second_range=Entry(self.subframe_tab_correlation, textvariable=self.residues_end_second_range ,width=30, font=("Courrier", 10))

            self.entry_residues_end_second_range.grid(row=7, column= 3)

            label_frames = Label(self.subframe_tab_correlation, text="FRAMES:", font=("Courrier", 12) ,width=30)
            label_frames.grid(row=8,column=2)

            label_frames_start = Label(self.subframe_tab_correlation, text="FROM", font=("Courrier", 10) ,width=30)
            label_frames_start.grid(row=9,column=2)

            label_frames_end = Label(self.subframe_tab_correlation, text="TO", font=("Courrier", 10)      ,width=30)
            label_frames_end.grid(row=9,column=3)
            
            
            
            label_threshold_pairs = Label(self.subframe_tab_correlation, text="Display residue pairs with " +'\n'+"Pearson coefficient:", font=("Courrier", 10)     ,width=30)
            label_threshold_pairs.grid(row=20,column=3)
            self.order_magnitude = StringVar()
            radiobutton_lower= Radiobutton(self.subframe_tab_correlation, text="lower than", variable= self.order_magnitude , value= "lower",  width=10)
            radiobutton_lower.grid(row=21, column=3)
            radiobutton_higher= Radiobutton(self.subframe_tab_correlation, text="higher than", variable=self.order_magnitude, value= "higher",  width=10)
            radiobutton_higher.grid(row=22, column=3)
            self.threshold_coeff= DoubleVar()
            self.entry_frames_threshold=Entry(self.subframe_tab_correlation, textvariable=self.threshold_coeff, width=20, font=("Courrier", 10))
            self.entry_frames_threshold.grid(row=23, column= 3)
            button_threshold = Button(self.subframe_tab_correlation, text="Apply threshold", font=("Courrier", 10)
                              ,width=30, command= self.threshold_pearson)
            button_threshold.grid(row=24,column=3)

   
            self.frames_start= IntVar()
            self.entry_frames_start=Entry(self.subframe_tab_correlation, textvariable=self.frames_start, width=30, font=("Courrier", 10))
            self.entry_frames_start.delete(0, END)

            self.entry_frames_start.grid(row=10, column= 2)
      
            self.frames_end= IntVar()
            self.entry_frames_end=Entry(self.subframe_tab_correlation, textvariable=self.frames_end, width=30, font=("Courrier", 10))
            self.entry_frames_end.delete(0, END)

            self.entry_frames_end.grid(row=10, column= 3)
            
            vmd_button = Button(self.subframe_tab_correlation, text="See on VMD", font=("Courrier", 10)
                              ,width=30, command= self.launch_vmd_from_smd_interface)
            vmd_button.grid(row=40,column=2)

            
            label_corr_movie = Label(self.subframe_tab_correlation, text="See the correlation heatmap"+'\n'+" every N frame (provide N)", font=("Courrier", 10)      ,width=30)
            label_corr_movie.grid(row=41,column=2)

            self.corr_map_window= IntVar()
            window_corrmap=Entry(self.subframe_tab_correlation, textvariable=self.corr_map_window, width=30, font=("Courrier", 10))
            window_corrmap.grid(row=42, column= 2)
            
            
            self.export_format = StringVar()

            radiobutton_gif= Radiobutton(self.subframe_tab_correlation, text="GIF", variable= self.export_format , value= "gif",  width=10)
            radiobutton_gif.grid(row=43, column=2)
            radiobutton_video= Radiobutton(self.subframe_tab_correlation, text="MP4", variable=self.export_format, value= "video",  width=10)
            radiobutton_video.grid(row=44, column=2)

            export_animation_button = Button(self.subframe_tab_correlation, text="Export animation", font=("Courrier", 10)
                              ,width=30, command= self.generate_animation)
            export_animation_button.grid(row=45,column=2)

    def calculate_correlations(self):   
        """
        This function calculates the correlations between the pairs of residues
        it measures the covariance between the force and distance
        whenever there is a change in the distance between a pair of residue
        how does that impact the force, positive correlations are colored in blue
        and negative correlations are colored in red
        """
        if self.widget_correlations:
            self.widget_correlations.destroy()

        if self.toolbar_correlations:
            self.toolbar_correlations.destroy()
        
        if self.residues_start.get() == 0:
            tkinter.messagebox.showinfo("Warning ", "Residue number can not be 0 ! ")      

        self.pb3 = ttk.Progressbar(self.subframe_tab_correlation, orient=HORIZONTAL, length=300,  mode='determinate')
        self.pb3.grid(row=2, columnspan=3, pady=20)
        for i in range(3):
            self.subframe.update_idletasks()
            self.pb3['value'] += 30
            time.sleep(1)

        n =  len(self.df['F[pN]']) // self.frames_end.get()
        window = ( 1/ n) * np.ones(n,)
        frc = np.convolve(self.df['F[pN]'], window, mode='valid')[::n]

        if self.residues_start_second_range.get()!=0 and self.residues_end_second_range.get() !=0:

            self.distances_pairs_residues_second_range , self.residue_names_second_range = corr.calculate_distances_between_residues(self.pdb, self.dcd, list(range(self.residues_start_second_range.get(), self.residues_end_second_range.get()+1)), self.frames_start.get(), self.frames_end.get())
 
            self.correlations_two = corr.calculate_correlations(self.distances_pairs_residues_second_range, frc, self.cutoff.get())       

            self.distances_pairs_residues_one_range, self.residue_names_one_range = corr.calculate_distances_between_residues(self.pdb, self.dcd, list(range(self.residues_start.get(), self.residues_end.get()+1)), self.frames_start.get(), self.frames_end.get())
            self.correlations_one = corr.calculate_correlations(self.distances_pairs_residues_one_range, frc, self.cutoff.get())
        
        elif self.residues_start.get() != 0 and self.residues_end.get() !=0 :

            self.distances_pairs_residues_one_range, self.residue_names_one_range = corr.calculate_distances_between_residues(self.pdb, self.dcd, list(range(self.residues_start.get(), self.residues_end.get()+1)), self.frames_start.get(), self.frames_end.get())
            self.correlations_one = corr.calculate_correlations(self.distances_pairs_residues_one_range, frc, self.cutoff.get())
        else:
            tkinter.messagebox.showinfo("Warning ", "Please end ranges properly")

        try:
            self.correlations_two
            self.correlation_matrix, self.residues= corr.plot_correlation_heatmap_multiple(self.correlations_one, self.correlations_two)
           
        except:
            self.correlation_matrix, self.residues= corr.plot_correlation_heatmap_multiple(self.correlations_one)

        figure =Figure(figsize=(12,10), dpi=80)
        ax= figure.add_subplot(111)

        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_correlation)
        canvas.draw()
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_correlation)
        toolbarFrame.grid(row=3,column=0)
  
        self.widget_correlations= canvas.get_tk_widget()
        self.widget_correlations.grid(row=2, column=0)
        
        self.toolbar_correlations = NavigationToolbar2Tk(canvas,  toolbarFrame) 
        self.toolbar_correlations.update()

        # Plot the correlation matrix as a heatmap
       # print(type(self.correlation_matrix))
        heatmap=ax.imshow(self.correlation_matrix,  cmap='bwr', vmin=-1, vmax=1)
        figure.colorbar(heatmap, label='Correlation Coefficient') 
        #ticks according to protein length so that the x and y label are readable
        residues=[]
        if len(self.residues) > 30 and len(self.residues) < 300:
            ax.set_xticks(np.arange(0, len(self.residues), 10))
            ax.set_yticks(np.arange(0, len(self.residues), 10 ))
            for i in range(0,len(self.residues), 10):
                residues.append(self.residues[i])
            ax.set_xticklabels(residues, rotation='vertical')
            ax.set_yticklabels(residues)
            
            
        elif len(self.residues) >  300:
            ax.set_xticks(np.arange(0, len(self.residues), 100))
            ax.set_yticks(np.arange(0, len(self.residues), 100 ))
            for i in range(0,len(self.residues), 100):
                residues.append(self.residues[i])
            ax.set_xticklabels(residues, rotation='vertical')
            ax.set_yticklabels(residues)
            

        else:
            ax.set_xticks(np.arange(len(self.residues)))
            ax.set_yticks(np.arange(len(self.residues)))
            ax.set_xticklabels(self.residues, rotation='vertical')
            ax.set_yticklabels(self.residues)
        ax.set_xlabel('Residue')
        ax.set_ylabel('Residue')
        self.pb3.destroy()
        
    def launch_vmd_from_smd_interface(self):
        corr.launch_vmd(self.pdb, self.dcd)

    def export_correlations(self):
        """
        This functions exports the correlations values 
        """
        import pandas as pd
        export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
        df_corr = pd.DataFrame(self.correlation_matrix)
        df_corr.index = self.residues
        df_corr.columns = self.residues
        df_corr.to_csv(export_file_path)
        print("CSV file saved successfully! ", export_file_path)

    def threshold_pearson(self):
        threshold=self.threshold_coeff.get()
        magnitude= self.order_magnitude.get()
        try:
            threshold
            magnitude
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, provide a threhold between [-1, 1] and select higher or lower")
        try:
            self.correlation_matrix
        except:
            self.calculate_correlations()
        
        df = pd.DataFrame(self.correlation_matrix)
        df.index = self.residues
        df.columns = self.residues
        if magnitude =="higher":
            filtered_df = df.where(df > threshold).dropna(how='all')

        if magnitude== "lower":
            filtered_df = df.where(df < threshold).dropna(how='all')
            

        print(filtered_df)
        export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')

        filtered_df.to_csv(export_file_path)
        tkinter.messagebox.showinfo("Information ", "CSV file with pairs of residues "+ str(magnitude)+ " than "+str(threshold)+ " was saved propely")
        

    def generate_animation(self):
        """
        This function generates an animation of the correlation map either in MP4 or GIF format
        the ouput of correlation maps is every N frames
        pdb_file: str
            pdb file
        dcd_file: str
            trajectory file from NAMD simulations
        residue_start:  str
            tells from which residue to start the calculation
        residue_end: str
            tells at which residue to stop
        start_frame: int
            tells at which frame start
        end_frame: int
            tells at which frame to stop
        force: list
            list containing force values 
        cuttoff: int
            cutoff distance (less than this distance threshold; the correlations are not calculated)
        outputfile_name: str
            name of output file
        N: int
            number of windows, every N window the correlation map is calculated
        outputype: str
            if GIF or MP4
        """
        import matplotlib.pyplot as plt
        frames = []
        N= self.corr_map_window.get()
        start_frame= self.frames_start.get()
        end_frame= self.frames_end.get()
        cutoff= self.cutoff.get()

        if self.export_format.get() == "":
            tkinter.messagebox.showinfo("Warning", "Please, select the file type (GIF or Video)")
            return

        else:
            output_type= self.export_format.get() 

        try:
            if self.force_tcl_tab2.get() == 1:
                force = self.df['-F[pN]']
            else:
                force = self.df['F[pN]']
        except KeyError:
            tkinter.messagebox.showinfo("Warning", "No tcl forces available; please unselect F(-v) radiobutton in 2nd tab")
            return
          #  force = self.df['F[pN]']



        progress_bar = ttk.Progressbar(self.subframe_tab_correlation, orient=HORIZONTAL, length=300,  mode='determinate')
        progress_bar.grid(row=48, column=2, pady=20)
        for i in range(3):
            self.subframe_tab_correlation.update_idletasks()
            progress_bar['value'] += 30
            time.sleep(1)
        for index in np.arange(start_frame, end_frame, N):
            frame_start = index
            frame_end = index + N
            print(f"Processing frames {frame_start} to {frame_end}")
            distances, residue_names = corr.calculate_distances_between_residues(self.pdb, self.dcd, list(range(int(self.residues_start.get()), int(self.residues_end.get()))), frame_start, frame_end)
            n = len(force) // frame_end
            window = (1 / n) * np.ones(n)
            frc = np.convolve(force, window, mode='valid')[::n]
            correlation = corr.calculate_correlations(distances, frc, cutoff)
            correlation_matrix, residues = corr.plot_correlation_heatmap(correlation)
            print(correlation_matrix)
            
            plt.imshow(correlation_matrix, cmap='bwr', vmin=-1, vmax=1)
            plt.xlabel('Residues')
            plt.ylabel('Residues')
            plt.title(f"Correlation Heatmap: Frames {frame_start} to {frame_end}")
            filename = f"{str(index).zfill(4)}.png"
            plt.savefig(filename)
            plt.close()
            frames.append(filename)

        
        if output_type == 'gif':
            output_file_name = filedialog.asksaveasfilename(defaultextension='.gif')
            with imageio.get_writer(output_file_name, mode='I', duration=end_frame) as writer:
                for frame in frames:
                    image = imageio.imread(frame)
                    writer.append_data(image)
        elif output_type == 'video':
            output_file_name = filedialog.asksaveasfilename(defaultextension='.mp4')
            with imageio.get_writer(output_file_name, fps=30, codec='libx264') as writer:
                for frame in frames:
                    image = imageio.imread(frame)
                    
                    # Debugging: Print the shape of the image
                    print(f"Original image shape: {image.shape}")
            
                    # Ensure the image is in RGB format
                    if len(image.shape) == 2:  # Grayscale image (height, width)
                        # Convert grayscale to RGB
                        image = np.stack([image] * 3, axis=-1)  # Shape will be (height, width, 3)
                    
                    elif len(image.shape) == 3:
                        if image.shape[-1] == 4:  # If RGBA
                            image = image[:, :, :3]  # Convert to RGB (remove alpha)
                        elif image.shape[-1] == 1:  # Grayscale with an extra dimension
                            image = np.concatenate([image] * 3, axis=-1)  # Convert to RGB
                        elif image.shape[-1] == 3:  # Already RGB
                            pass  # No changes needed
            
                    # Final check for shape
                    if image.shape[-1] != 3:
                        print(f"Warning: Image does not have 3 channels: {image.shape}")
            
                    print(f"Processed image shape: {image.shape}")
            
                    # Append the image to the video writer
                    writer.append_data(image)

        else:
            print("Invalid output type. Please choose 'gif' or 'video'.")

        for frame in frames:
            os.remove(frame)
        progress_bar.destroy()
        print(f"Animation saved in {output_type} format")
        tkinter.messagebox.showinfo("Information", "Your file has been saved as "+ output_type +" properly")


    def create_buttons_work(self):
        """
        creates all buttons of the last tab: compute correlations between pairs of residues
        """

        calculate_button = Button(self.subframe_tab_work, text="1- Calculate the work", font=("Courrier", 10)
                          ,width=30, command= self.calculate_work)
        calculate_button.grid(row=20,column=2)


        export_button = Button(self.subframe_tab_work, text="2- Export work values", font=("Courrier", 10)  ,width=30, command=self.export_correlations)
        export_button.grid(row=30,column=2)

        label_residues = Label(self.subframe_tab_work, text="RESIDUES:", font=("Courrier", 12) ,width=30)
        label_residues.grid(row=4,column=2)
          
        label_residues_start = Label(self.subframe_tab_work, text="FROM", font=("Courrier", 10) ,width=30)
        label_residues_start.grid(row=5,column=2)
 
        label_residues_end = Label(self.subframe_tab_work, text="TO", font=("Courrier", 10)     ,width=30)
        label_residues_end.grid(row=5,column=3)

        self.residues_start_work= IntVar()
        self.entry_residues_start_work=Entry(self.subframe_tab_work, textvariable=self.residues_start_work, width=30, font=("Courrier", 10))
        self.entry_residues_start_work.delete(0, END)
        self.entry_residues_start_work.grid(row=6, column= 2)

        self.residues_end_work= IntVar()
        self.entry_residues_end_work=Entry(self.subframe_tab_work, textvariable=self.residues_end_work, width=30, font=("Courrier", 10))
        self.entry_residues_end_work.delete(0, END)

        self.entry_residues_end_work.grid(row=6, column= 3)


        label_frames = Label(self.subframe_tab_work, text="FRAMES:", font=("Courrier", 12) ,width=30)
        label_frames.grid(row=8,column=2)

        label_frames_start = Label(self.subframe_tab_work, text="FROM", font=("Courrier", 10) ,width=30)
        label_frames_start.grid(row=9,column=2)

        label_frames_end = Label(self.subframe_tab_work, text="TO", font=("Courrier", 10)      ,width=30)
        label_frames_end.grid(row=9,column=3)

   
        self.frames_start_work= IntVar()
        self.entry_frames_start_work=Entry(self.subframe_tab_work, textvariable=self.frames_start_work, width=30, font=("Courrier", 10))
        self.entry_frames_start_work.delete(0, END)

        self.entry_frames_start_work.grid(row=10, column= 2)
  
        self.frames_end_work= IntVar()
        self.entry_frames_end_work=Entry(self.subframe_tab_work, textvariable=self.frames_end_work, width=30, font=("Courrier", 10))
        self.entry_frames_end_work.delete(0, END)

        self.entry_frames_end_work.grid(row=10, column= 3)
        
        label_frames_window = Label(self.subframe_tab_work, text="Calculate work every N frame: ", font=("Courrier", 10)      ,width=30)
        label_frames_window.grid(row=40,column=2)
        
        self.work_map_window= IntVar()
        window_corrmap=Entry(self.subframe_tab_work, textvariable=self.work_map_window, width=30, font=("Courrier", 10))
        window_corrmap.grid(row=42, column= 2)
        

        export_animation_button = Button(self.subframe_tab_work, text="Export map as GIF", font=("Courrier", 10)
                          ,width=30, command= self.generate_animation_work)
        export_animation_button.grid(row=45,column=2)
            
    def calculate_work(self):
        """
        This function calculates the work between the pairs of residues
     
        """
        if self.widget_work:
            self.widget_work.destroy()

        if self.toolbar_work:
            self.toolbar_work.destroy()
        
        if self.residues_start_work.get() == 0:
            tkinter.messagebox.showinfo("Warning ", "Residue number can not be 0 ! ")      

        self.pb_work = ttk.Progressbar(self.subframe_tab_work, orient=HORIZONTAL, length=300,  mode='determinate')
        self.pb_work.grid(row=2, columnspan=3, pady=20)
        for i in range(3):
            self.subframe_tab_work.update_idletasks()
            self.pb_work['value'] += 30
            time.sleep(1)

        
        start= int(self.frames_start_work.get())
        end= int(self.frames_end_work.get())
        residue_ids= list(range(self.residues_start_work.get(), self.residues_end_work.get()))
        
        if self.residues_start_work.get() != 0 and self.residues_end_work.get() !=0 :
            self.work_per_pair = corr.calculate_work(self.pdb, self.dcd, self.force_vector, start, end, residue_ids)
            self.heatmap_data= corr.plot_work_heatmap(self.work_per_pair, residue_ids)
            print(self.heatmap_data)
        else:
            tkinter.messagebox.showinfo("Warning ", "Please end ranges properly")


        figure = Figure(figsize=(12,10), dpi=80)
        ax= figure.add_subplot(111)

        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_work)
        canvas.draw()
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_work)
        toolbarFrame.grid(row=3,column=0)
  
        self.widget_work= canvas.get_tk_widget()
        self.widget_work.grid(row=2, column=0)
        
        self.toolbar_work = NavigationToolbar2Tk(canvas,  toolbarFrame) 
        self.toolbar_work.update()
        min_val= int(np.abs(np.min(self.heatmap_data)))
        max_val= int(np.max(self.heatmap_data))
        range_map= max(min_val, max_val)
        heatmap=ax.imshow(self.heatmap_data,  cmap='bwr', vmin=-range_map, vmax= range_map)
        figure.colorbar(heatmap, label='Work (Joules)') 
        residues=[]
        if len(residue_ids) > 30 and len(residue_ids) < 300:
            ax.set_xticks(np.arange(0, len(residue_ids), 10))
            ax.set_yticks(np.arange(0, len(residue_ids), 10 ))
            for i in range(0,len(residue_ids), 10):
                residues.append(residue_ids[i])
            ax.set_xticklabels(residues, rotation='vertical')
            ax.set_yticklabels(residues)
            
            
        elif len(residue_ids) >  300:
            ax.set_xticks(np.arange(0, len(residue_ids), 100))
            ax.set_yticks(np.arange(0, len(residue_ids), 100 ))
            for i in range(0,len(residue_ids), 100):
                residues.append(residue_ids[i])
            ax.set_xticklabels(residues, rotation='vertical')
            ax.set_yticklabels(residues)
            

        else:
            ax.set_xticks(np.arange(len(residue_ids)))
            ax.set_yticks(np.arange(len(residue_ids)))
            ax.set_xticklabels(residue_ids, rotation='vertical')
            ax.set_yticklabels(residue_ids)
        ax.set_xlabel('Residue')
        ax.set_ylabel('Residue')
        self.pb_work.destroy()
        
        
    def generate_animation_work(self):
        import matplotlib.pyplot as plt
        frames = []
        N= self.work_map_window.get()
        start_frame= self.frames_start_work.get()
        end_frame= self.frames_end_work.get()
        residue_ids= list(range(self.residues_start_work.get(), self.residues_end_work.get()))
        
        progress_bar_wgif = ttk.Progressbar(self.subframe_tab_work, orient=HORIZONTAL, length=300,  mode='determinate')
        progress_bar_wgif.grid(row=48, column=2, pady=20)

        for i in range(3):
            self.subframe_tab_work.update_idletasks()
            progress_bar_wgif['value'] += 30
            time.sleep(1)
        for index in np.arange(start_frame, end_frame, N):
            frame_start = index
            frame_end = index + N
            print(f"Processing frames {frame_start} to {frame_end}")
            self.work_per_pair = corr.calculate_work(self.pdb, self.dcd, self.force_vector, frame_start, frame_end, residue_ids)
            self.heatmap_data= corr.plot_work_heatmap(self.work_per_pair, residue_ids)
            min_val= int(np.abs(np.min(self.heatmap_data)))
            max_val= int(np.max(self.heatmap_data))
            range_map= max(min_val, max_val)
            #     sns.heatmap(heatmap_data,  cmap='coolwarm', 
            #                 xticklabels=residue_ids, yticklabels=residue_ids,
            #                 cbar_kws={'label': 'work'})

            im= plt.imshow(self.heatmap_data,  cmap='bwr', vmin=-range_map, vmax= range_map)
            cbar = plt.colorbar(im)
            cbar.set_label("Work (Joules)")
            # figure.colorbar(heatmap, label='Work (Joules)') 
            plt.xlabel('Residues')
            plt.ylabel('Residues')
            plt.title(f"Work Heatmap: Frames {frame_start} to {frame_end}")
            filename = f"{str(index).zfill(4)}.png"
            plt.savefig(filename)
            plt.close()
            frames.append(filename)

        
        output_file_name = filedialog.asksaveasfilename(defaultextension='.gif')
        with imageio.get_writer(output_file_name, mode='I', duration=end_frame) as writer:
            for frame in frames:
                image = imageio.imread(frame)
                writer.append_data(image)
        for frame in frames:
            os.remove(frame)
        progress_bar_wgif.destroy()
        tkinter.messagebox.showinfo("Information", "Your file has been saved properly")

app = App()
app.window.mainloop()